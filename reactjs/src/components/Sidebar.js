import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import '../css/main.css';
import { 
  SET_ACTIVE_MENU
} from '../constants/actionTypes';

const LoggedOutView = data => {
  if (!data.currentUser) {
    return null;
  } else {
    return null;
  }
};

const LoggedInView = data => {
  const visitadores = data && data.currentUser && data.currentUser.grupos && data.currentUser.grupos.find(g => g.grupo === 'visitadores')  ? true: false;

  if (data.currentUser) {
    return (
      <div>
        <div id="barra">
          <ul id="menu">
            <li id="logo">
              <strong>
                ACTAS USE
              </strong>
            </li>
            <li className={data.activo === 'lotes'? 'actv': ''}>
              <Link to="lotes">
                Lotes
              </Link>
            </li>
            { !visitadores?
              <li className={data.activo === 'distribucion_regular'? 'actv': ''}>
                <Link to="distribucion_regular">
                  Preparar distribución 
                  <p className="label-periodo"><small>(mensual)</small></p>
                </Link>
              </li> : null }
            { !visitadores?
              <li className={data.activo === 'entrega_regular'? 'actv': ''}>
                <Link to="entrega_regular">
                  Asignar folios
                  <p className="label-periodo"><small>(mensual)</small></p>
                </Link>
              </li> : null }
            
            <li className={data.activo === 'entrega_especial'? 'actv': ''}>
              <Link to={visitadores? "entregas" : "entrega_especial"}>
                Entregas
                <p className="label-periodo"><small>(diario)</small></p>
              </Link>
            </li>
          </ul>
          <ul id="totales">
            <li>
              <span>
                Actas disponibles:
              </span>
              <br/>              
              <strong>
                {data.total_disponible}
              </strong>
            </li>
            <li>
              <span>
                Total de actas:
              </span>
              <br/>
              <strong>
                {data.total_actas}
              </strong>
            </li>
          </ul>
        </div>
      </div>
    );
  }
  else {
    return null;
  }
};

const mapStateToProps = state => ({
  activo: state.common.active,
  currentUser: state.common.currentUser,
  total_actas: state.lotes.ta,
  total_disponible: state.lotes.td
});
const mapDispatchToProps = dispatch => ({
  setActive: (menu) => dispatch({ type: SET_ACTIVE_MENU, menu }),
});
class Sidebar extends React.Component {
  render() {
    const { activo, currentUser, total_actas, total_disponible } = this.props;
    console.log(activo);
    return (
      <div>
        <LoggedOutView currentUser={currentUser} activo={activo} />
        <LoggedInView currentUser={currentUser} activo={activo} total_actas={total_actas} total_disponible={total_disponible} />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);