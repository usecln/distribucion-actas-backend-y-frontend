import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { Dimmer, Loader } from 'semantic-ui-react'

import {
  LOTES_PAGE_LOADED,
  LOTES_PAGE_UNLOADED
} from '../constants/actionTypes';

import '../css/lotes.css';

const mapStateToProps = state => ({
  ...state.lotes,
  currentUser: state.common.currentUser,
  inProgress: state.settings.inProgress
});

const mapDispatchToProps = dispatch => ({
  onLoad: payload => dispatch({ type: LOTES_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: LOTES_PAGE_UNLOADED }),
});

class Lotes extends React.Component {
  last_login = '';

  componentWillMount() {
    if(!this.props.currentUser)
      window.location.replace('/');
    
    this.props.onLoad(Promise.all([
      agent.Lotes.getLotes(this.props.currentUser.id),
    ]));
    const dt = window.localStorage.getItem('last_login') || new Date();
    this.last_login = new Date(dt).toJSON().slice(0,10).split('-').reverse().join('/');
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { currentUser, lotes } = this.props;
    const { last_login } = this;
    return (
      <div style={{ position: 'relative' }}>
        <Dimmer active={!lotes} page>
          <Loader size='medium'>Cargando lotes disponibles</Loader>
        </Dimmer>
          <div id="login-info">
            <p className="info">
              Bienvenido <b>{currentUser.username}</b> <span>|</span> último inicio de sesión: <b>{last_login}</b>
            </p>
          </div>
            { !lotes || (lotes && lotes.length === 0)?
            <div id="sin-lotes">
              <h3>No hay lotes disponibles</h3>
            </div>
          : 
          <div className="lotes">
            <table className="listing" role="grid">
              <thead>
                <tr className="titles tablesorter-headerRow" role="row">
                  <td>
                    Id
                  </td>
                  <td>
                    Tipo
                  </td>
                  <td>
                    Folio inicial
                  </td>
                  <td>
                    Folio final
                  </td>
                  <td>
                    Ultimo folio asignado
                  </td>
                  <td>
                    Total de actas
                  </td>
                  <td>
                    Actas disponibles
                  </td>
                </tr>
              </thead>
              <tbody aria-live="polite" aria-relevant="all">
                { lotes.map(l => [
                  <tr className="req-item">
                    <td>
                      {l.tipo.tipo.toUpperCase().charAt(0) + l.id}
                    </td>
                    <td>
                      {l.tipo.tipo}
                    </td>
                    <td>
                      {l.folio_inicial}
                    </td>
                    <td>
                      {l.folio_final}
                    </td>
                    <td>
                      { Number(l.ultimo_folio) === -1? 'NA': l.ultimo_folio }
                    </td>
                    <td>
                      {l.total_actas}
                    </td>
                    <td className="calif">
                      <span className="calif calif-B3">{l.disponibles}</span>
                    </td>
                  </tr>,
                  <tr className="progress">
                    <td colSpan={11}>
                        <div className="bar" style={{width: l.progress + '%'}}></div>
                    </td>
                  </tr>,
                  <tr className="space">
                    <td colSpan={11}></td>
                  </tr>
                ]
                )}
              </tbody>
            </table>
          </div>}
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Lotes);