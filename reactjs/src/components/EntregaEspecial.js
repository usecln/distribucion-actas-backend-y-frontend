import React from 'react';
import agent from '../agent';
import moment from 'moment';
import printJS from 'print-js';
// import jsPDF from 'jspdf';
import 'moment/locale/es';
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';
import { SingleDatePicker } from 'react-dates';
import { connect } from 'react-redux';
import { Form, Button, Segment, Dimmer, Loader, Icon, Modal, Header } from 'semantic-ui-react'
import {
  ASYNC_START,
  ASYNC_END,
  EE_PAGE_LOADED,
  EE_PAGE_UNLOADED,
  EE_PAGE_INIT,
  EE_PAGE_LOAD_FOLIOS,
  EE_PAGE_LOAD_TIPO_LOTES,
  EE_PAGE_ACTUALIZA_ENTREGAS,
  EE_PAGE_ACTUALIZA_TIPO_FOLIO,
  REFRESH_LOTES
} from '../constants/actionTypes';

moment.locale('es');
const mapStateToProps = state => ({
  ...state.entrega_especial,
  total_actas: state.lotes.ta,
  folios_disponibles_por_tipo: state.lotes.folios_disponibles_por_tipo,
  total_disponible: state.lotes.td,
  currentUser: state.common.currentUser,
  inProgress: state.settings.inProgress
});

const mapDispatchToProps = dispatch => ({
  onLoad: payload => dispatch({ type: EE_PAGE_LOADED, payload }),
  onUnload: () => dispatch({ type: EE_PAGE_UNLOADED }),
  init: payload => dispatch({ type: EE_PAGE_INIT, payload }),
  onLoadTipoLotes: payload => dispatch({ type: EE_PAGE_LOAD_TIPO_LOTES, payload }),
  onLoadFolios: payload => dispatch({ type: EE_PAGE_LOAD_FOLIOS, payload }),
  onTipoFolioChanged: payload => dispatch({ type: EE_PAGE_ACTUALIZA_TIPO_FOLIO, payload }),
  onDateChanged: payload => dispatch({ type: EE_PAGE_ACTUALIZA_ENTREGAS, payload }),
  onSaveStarted: () => dispatch({ type: ASYNC_START }),
  onSaveCanceled: () => dispatch({ type: ASYNC_END }),
  refreshLotes: payload => dispatch({ type: REFRESH_LOTES, payload }),
});

class EntregaEspecial extends React.Component {
  state = { error: false, cve_oficialia: null, actas_oficialia: '', actas_use: '', actas_interestatales: '' }
  downloadPDF() {
    var link = document.createElement("a");
    link.download = "distribucion.xlsx";
    link.href = agent.API_ROOT + '/api/v1/get_pdf/?distribucion=' + this.props.distribucion_id;
    link.click();
  }

  hoy() {
    if(!this) {
      return false;
    } else {
      console.log(this);
      const { dia, mes, ano } = this.props;
      const nd = new Date();
      return Number(mes) === nd.getMonth() && Number(dia) === nd.getDate() && Number(ano) === nd.getFullYear();
    }
  }
  borraDistribucion() {
    const { onSaveStarted, distribucion_id, dia, mes, ano, refreshLotes } = this.props;
    onSaveStarted();
    this.setState({ cve_oficialia: null, actas_oficialia: '', actas_use: '', actas_interestatales: '', folios_entregar: '' });
    Promise.resolve(agent.Distribucion.borraDistribucion(distribucion_id)).then(() => {
      this.cambiaFecha(moment(new Date(ano, mes, dia).getTime()));
      refreshLotes(Promise.all([
        agent.Lotes.getLotes(this.props.currentUser.id),
      ]));
    });
  }
  getFecha() {
    const { dia, mes, ano } = this.props;
    if(isNaN(Number(dia)) || isNaN(Number(mes)) || isNaN(Number(ano))) {
      return moment();
    }
    else {
      return moment(new Date(ano, mes, dia).getTime());
    }
  }
  imprimeComprobante() {
    const { dia, mes, ano, distribucion_id } = this.props;
    printJS({
      printable:  `${agent.API_ROOT}/api/v1/get_entrega/?dia=${dia}&mes=${mes}&ano=${ano}&distribucion=${distribucion_id}`,
      type: 'pdf',
      showModal: true,
      modalMessage: ''
    });
  }
  error(mensaje) {
    if(mensaje.indexOf('duplicate key') !== -1)
      mensaje = 'Ha ocurrido un error, ya se entregaron folios de este tipo de folios, a esta oficialía, el día de hoy, solo se pueden entregar folios una vez al día por oficialía, puede entregarle folios de otro tipo de folios, si entregó menos o más folios de los necesarios a esta oficialía pruebe eliminando esa entrega y entregando de nuevo la cantidad correcta';
    this.setState({ error: true, mensaje });
  } 
  pendientesAsignar() {
    const { oficialias } = this.props;
    const sin_asignar = ( oficialias && oficialias.filter(o => !o.folio_inicial && !o.folio_inicial1) ) || [];
    console.log(sin_asignar)
    return sin_asignar.length > 0;
  }
  asignaFolios(entregas) {
    const { currentUser, dia, mes, ano, onSaveCanceled, refreshLotes } = this.props;
    const { tipo_lote } = this.state;

    agent.Entrega.asignaFolios(entregas, currentUser.username, tipo_lote).then(f => {
      if(f.error) {
        this.error(f.error);
      }
      onSaveCanceled();
      this.cambiaFecha(moment(new Date(ano, mes, dia).getTime()));
      refreshLotes(Promise.all([
        agent.Lotes.getLotes(this.props.currentUser.id),
      ]));
    });
  }
  agregaEntrega() {
    const { currentUser, dia, mes, ano, onSaveStarted, onSaveCanceled, folios_disponibles_por_tipo, visitador } = this.props;
    const { folios_entregar, cve_oficialia, actas_oficialia, actas_use, actas_interestatales, tipo_lote } = this.state;
    console.log(this.state);
    console.log(folios_disponibles_por_tipo);
    
    if((Number(folios_entregar) + Number(actas_oficialia) + Number(actas_use) + Number(actas_interestatales)) > Number(folios_disponibles_por_tipo.replace(',', ''))) {
      this.error('No hay folios suficientes para realizar la entrega solicitada, cargue más lotes de actas en el sistema antes de continuar.');
      return;
    }


    agent.Distribucion.distribucion(currentUser.id, dia, mes + 1, ano).then(d => {

      if(d.objects.length === 0) {
        agent.Distribucion.createDistribucion(currentUser.id, dia, mes + 1, ano).then(dd => {
          agent.Distribucion.distribucion(currentUser.id, dia, mes + 1, ano).then(d => {
            let distribucion = d.objects[0];
            window.scrollTo(0, 0);
            onSaveStarted();
            var entregas = [{
              distribucion: distribucion.resource_uri,
              oficialia: '/api/v1/oficialia/' + cve_oficialia + '/',
              actas_oficialia: visitador? Number(folios_entregar) : Number(actas_oficialia), // si es visitador los folios que entrega se guardan en el campo actas_oficialia
              actas_use: Number(actas_use),
              actas_interestatales: Number(actas_interestatales),
              user: '/auth/v1/user/' + currentUser.id + '/',
              tipo: '/api/v1/tipo/' + tipo_lote  +'/'
            }];
            this.setState({ cve_oficialia: null, actas_oficialia: '', actas_use: '', actas_interestatales: '', folios_entregar: '' });
            // agregar entrega
            agent.Distribucion.addEntregas({ objects: entregas }).then(e => {
              agent.Distribucion.distribucion(currentUser.id, dia, mes + 1, ano).then(d => {
                // asignar folios
                this.asignaFolios(d.objects[0].entregas.map(ex => ex.id));
              });
            }).catch(err => {
              onSaveCanceled();
              this.error(err.response.body.error_message);
            });
          });
        });
      }
      else {
        let distribucion = d.objects[0];
        window.scrollTo(0, 0);
        onSaveStarted();

        var entregas = [{
          distribucion: distribucion.resource_uri,
          oficialia: '/api/v1/oficialia/' + cve_oficialia + '/',
          actas_oficialia: visitador? Number(folios_entregar) : Number(actas_oficialia),
          actas_use: Number(actas_use),
          actas_interestatales: Number(actas_interestatales),
          user: '/auth/v1/user/' + currentUser.id + '/',
          tipo: '/api/v1/tipo/' + tipo_lote  +'/'
        }];
        this.setState({ cve_oficialia: null, folios_entregar: '', actas_oficialia: '', actas_use: '', actas_interestatales: '' });
        // agregar entrega
        agent.Distribucion.addEntregas({ objects: entregas }).then(e => {
          // asignar folios
          agent.Distribucion.distribucion(currentUser.id, dia, mes + 1, ano).then(d => {          
            this.asignaFolios(d.objects[0].entregas.map(ex => ex.id));
          });
        }).catch(err => {
          onSaveCanceled();
          this.error(err.response.body.error_message);
        });
      }
    });
  }
  cambiaFecha(moment) {
    const { currentUser, onDateChanged, onLoadFolios, onSaveCanceled } = this.props;    
    this.setState({ cve_oficialia: null, actas_oficialia: '', actas_use: '', actas_interestatales: '' });
    let dt = new Date(moment.toDate());

    var d = dt.getDate(), m = dt.getMonth() , a = dt.getFullYear();
    Promise.all([
      agent.Distribucion.distribucion(currentUser.id, d, m + 1, a),
      d, m, a, agent.Lotes.getLotes(currentUser.id)
    ]).then(s => {
      onDateChanged(s);
      agent.Entrega.getFolios(this.props.entregas).then(obs => {
        onLoadFolios(obs);
        onSaveCanceled();
        console.log(this.props)
      })
      
    });
  }
  borrar(entrega_id) {
    console.log('borrar', entrega_id);
    this.borraDistribucion();
  }
  imprimir(entrega) {
    console.log('imprimir', entrega);
  }
  selectTipoFolios(tipo) {
    console.log(tipo);
    this.setState({ tipo_lote: tipo})
    this.props.onTipoFolioChanged(tipo);
  }
  componentWillMount() {
    const { currentUser, onLoad, lista_oficialias, init } = this.props;
    let cd = new Date();
    if(!currentUser)
      window.location.replace('/');

    const visitador = currentUser && currentUser.grupos && currentUser.grupos.find(g => g.grupo === 'visitadores')  ? true: false;
    onLoad(visitador);

    agent.Profile.getTipoLotes(currentUser.id).then(r => {
      if(r.objects && r.objects.length > 0) {
        const tipo = r.objects[0].id;
        this.selectTipoFolios(tipo);
      }
      this.props.onLoadTipoLotes(r.objects);
    }).catch(() => {});

    if(!lista_oficialias || (lista_oficialias && lista_oficialias.length === 0)) {
      agent.Distribucion.recaudaciones().then(r => {
        init(r.objects);
        this.cambiaFecha(moment(cd));
      }).catch(() => {});
    }
    else
      this.cambiaFecha(moment(cd));
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { imprimir } = this;
    const { cve_oficialia, tipo_lote, folios_entregar, actas_oficialia, actas_use, actas_interestatales, error, mensaje } = this.state;
    const { folios_disponibles_por_tipo, oficialias, inProgress, total, lista_oficialias, visitador, tipo_lotes, ready } = this.props;

    return (
      <div style={{ padding: '40px' }}>
        <Dimmer active={!ready} page>
          <Loader size='medium'>Cargando</Loader>
        </Dimmer>
        <Modal open={error} basic size='small'>
          <Header icon='browser' content='Error' />
          <Modal.Content>
            <h3 style={{ color: '#e62525' }}>{mensaje}</h3>
          </Modal.Content>
          <Modal.Actions>
            <Button color='green' onClick={() => this.setState({ error: false })} inverted>
              <Icon name='checkmark' />Ok
            </Button>
          </Modal.Actions>
        </Modal>
        <Segment inverted style={{ width: '100%'}}>
          <Form inverted style={{ height: '2em', position: 'static', width: '100%' }}>
            { (!visitador?  'Distribución especial': 'Entrega diaria') + ' (' + total + ' folios entregados este día) '}<br/>
            <span style={{ color: '#02c39e' }}>Folios disponibles ({folios_disponibles_por_tipo + ' - ' + ((tipo_lotes && tipo_lotes.find(t => t.key === tipo_lote)) || { text:'' }).text})</span>
            <div style={{ position: 'absolute', top: '0.6em', right: '1em' }}>
              <SingleDatePicker
                date={this.getFecha()}
                isOutsideRange={() => false}
                onDateChange={date => this.cambiaFecha(date)} 
                focused={this.state.focused} // PropTypes.bool
                onFocusChange={({ focused }) => this.setState({ focused })} // PropTypes.func.isRequired
              />
            </div>
          </Form>
        </Segment>
        <Dimmer active={inProgress} page>
          <Loader size='medium'></Loader>
        </Dimmer>
        { oficialias && oficialias.length > 0? 
            <table className="listing" role="grid" style={{ marginTop: '30px' }}>
              <thead>
                <tr className="titles tablesorter-headerRow" role="row">
                  <td>
                    Oficialia
                  </td>
                  { visitador? [
                    <td key="1">
                      Folios entregados
                    </td>,
                    <td key="2">
                      Tipo de folios
                    </td>,
                  ]: [
                      <td key="1">
                        Actas oficialia
                      </td>,
                      <td key="2">
                        Actas use
                      </td>,
                      <td key="3">
                        Actas interestatales
                      </td>
                    ]
                  }
                  <td>
                    Folio inicial
                  </td>
                  <td>
                    Folio final
                  </td>
                  <td>
                    Opciones
                  </td>
                </tr>
              </thead>
              <tbody aria-live="polite" aria-relevant="all">
                { 
                  oficialias.sort((x,y) => x.id - y.id).map(o => [
                    <tr className="req-item">
                      <td style={{ textAlign: 'left' }} key="1">
                        { (o.cve_oficialia? o.cve_oficialia + ' ': '') + o.nombre }<br/>
                      </td>
                      { visitador? [
                          <td key="1">
                            {o.actas_oficialia}
                          </td>,
                          <td key="2">
                            {o.tipo_lote}
                          </td>,
                        ]: [
                        <td>
                          {o.actas_oficialia}
                        </td>,
                        <td>
                          {o.actas_use}
                        </td>,
                        <td>
                          {o.actas_interestatales}
                        </td>
                      ] }
                      {
                        !o.folio_inicial && !o.folio_inicial1?
                          <td colSpan={2}>
                            <Button color="instagram" onClick={() => this.asignaFolios([o.entrega_id])} disabled={!this.hoy()}>
                              Asignar folios
                            </Button>
                          </td> : [
                            <td key="1">
                              { o.folio_inicial? (!visitador? 'A25 ': '') + o.folio_inicial: '' }
                              <span style={{ color: '#02c39e' }}>{ o.folio_inicial1? (!visitador? 'A25 ': '') + o.folio_inicial1: '' }</span><br />
                              <span style={{ color: '#f2711c' }}>{ o.folio_final2? (!visitador? 'A25 ': '') + o.folio_inicial2: ''}</span>
                            </td>,
                            <td key="2">
                              { o.folio_final? (!visitador? (!visitador? 'A25 ': ''): '') + o.folio_final: '' }
                              <span style={{ color: '#02c39e' }}>{ o.folio_inicial1? (!visitador? 'A25 ': '') + o.folio_final1: ''}</span><br />
                              <span style={{ color: '#f2711c' }}>{ o.folio_final2? (!visitador? 'A25 ': '') + o.folio_final2: ''}</span>
                            </td>
                        ]
                      }
                      <td>
                        <Button.Group icon>
                        { this.hoy()? 
                          <Button color="youtube" disabled={Math.max(...oficialias.filter(of => of.tipo_lote === o.tipo_lote).map(oo=>oo.id)) !== o.id} style={{background: '#e55441'}} onClick={() => this.borrar(o.id)}>
                            <Icon name='delete' />
                          </Button>
                        : null }
                          <Button color="instagram" onClick={() => imprimir(o)} disabled={!o.folio_inicial && ! o.folio_inicial1}>
                            <Icon name='print' />
                          </Button>
                        </Button.Group>
                      </td>
                    </tr>,
                    <tr style={{ height: '10px' }}/>
                  ])
                }
              </tbody>
            </table>
          : <div className="sin-entregas">
              <h3>
                { this.getFecha() > moment()? 'No se puede entregar folios en fechas superiores a la fecha actual': 'No se entregaron folios este día' }
              </h3>
            </div>
        }
        { 
          this.hoy() && !this.pendientesAsignar()? // si se selecciona la fecha igual a la fecha actual
          <Form>
            <Form.Dropdown label='Oficialia' placeholder='Oficialia' fluid search selection options={lista_oficialias || []} onChange={(e, { value }) => this.setState({ cve_oficialia: value}) } value={cve_oficialia}/>
            <Form.Group widths='equal'>
            { 
              visitador? 
                <Form.Input type="number" fluid label='Folios a entregar' placeholder='Folios a entregar' value={folios_entregar} onChange={(e, { value }) => this.setState({ folios_entregar: value}) } />: 
              [
                <Form.Input type="number" fluid label='Actas oficialia' placeholder='Actas oficialia' value={actas_oficialia} onChange={(e, { value }) => this.setState({ actas_oficialia: value}) } />,
                <Form.Input type="number" fluid label='Actas use' placeholder='Actas use' value={actas_use} onChange={(e, { value }) => this.setState({ actas_use: value}) }/>,
                <Form.Input type="number" fluid label='Actas interestatales' placeholder='Actas interestatales' value={actas_interestatales} onChange={(e, { value }) => this.setState({ actas_interestatales: value}) } />
              ]
            }
              <Form.Dropdown label='Tipo de folios' placeholder='' fluid selection options={tipo_lotes || []} onChange={(e, { value }) => this.selectTipoFolios(value) } value={tipo_lote} disabled={tipo_lotes.length === 1}/>
            </Form.Group>
            <Form.Group>
              <Form.Button primary disabled={!cve_oficialia || !tipo_lote || (Number(folios_entregar) === 0 && Number(actas_oficialia) === 0 && Number(actas_use) === 0 && Number(actas_interestatales) === 0)} onClick={ (e, ) => this.agregaEntrega() }>
                Entregar
              </Form.Button>
            </Form.Group>
          </Form> : null }
        { !this.hoy() && oficialias && oficialias.length > 0? 
          <div className="sin-entregas">
            <h3>
              Las entregas realizadas en días anteriores no se pueden modificar.
            </h3>
          </div>: null
        }
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EntregaEspecial);