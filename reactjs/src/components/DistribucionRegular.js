import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { Accordion, Form, Input, Label, Button, Segment, Icon, Header, Modal, Dimmer, Loader, Popup, Select } from 'semantic-ui-react'

import {
  DR_PAGE_LOADED,
  DR_PAGE_UNLOADED,
  UPDATE_FIELD_OFICIALIA,
  DR_CONTRACT_FORMS,
  DR_EXPAND_FORMS,
  DR_TOGGLE_FORM,
  DR_DISTRIBUCION_SUGERIDA,
  DR_PAGE_LOAD_FOLIOS,
  DR_GUARDAR,
  ASYNC_START,
  ASYNC_END
} from '../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.distribucion,
  currentUser: state.common.currentUser,
  inProgress: state.settings.inProgress
});

const mapDispatchToProps = dispatch => ({
  onLoad: payload => dispatch({ type: DR_PAGE_LOADED, payload }),
  onLoadFolios: payload => dispatch({ type: DR_PAGE_LOAD_FOLIOS, payload }),
  onDistribucionSugerida: () => dispatch({ type: DR_DISTRIBUCION_SUGERIDA }),
  onChangeOficialia: oficialia => dispatch({ type: UPDATE_FIELD_OFICIALIA, oficialia }),
  onSaveStarted: () => dispatch({ type: ASYNC_START }),
  onSaveCanceled: () => dispatch({ type: ASYNC_END }),
  onSave: payload => dispatch({ type: DR_GUARDAR, payload }),
  onContract: () => dispatch({ type: DR_CONTRACT_FORMS }),
  onExpand: () => dispatch({ type: DR_EXPAND_FORMS }),
  onToggle: payload => dispatch({ type: DR_TOGGLE_FORM, recaudacion: payload }),
  onUnload: () => dispatch({ type: DR_PAGE_UNLOADED })
});

class DistribucionRegular extends React.Component {
  state = { open: false, cambiaMes: false, ano: (new Date().getFullYear()), mes: (new Date().getMonth() + 1) }

  show = () => () => this.setState({ open: true })
  showCambiaMes = () => () => this.setState({ cambiaMes: true })
  close = () => this.setState({ open: false })
  closeCambiaMes = () => this.setState({ cambiaMes: false })
  changeAno = () => e => {
    if(Number(e.target.value) < 3000)
      this.setState({ ano: e.target.value });
  }
  changeMes = y => e => {
    let m = this.props.meses.filter(m => m.mes === e.target.textContent);
    if(m.length > 0)
      this.setState({ mes: m[0].id });
  }
  cambiaFecha = () => {
    if(Number(this.state.ano) < 1980 || Number(this.state.ano) > 2500)
      alert("El campo de año solo permite valores entre 1980 y 2500");
    else
    {
      this.closeCambiaMes();
      this.props.onSaveStarted();
      agent.Distribucion.distribucion(this.props.currentUser.id, null, this.state.mes, this.state.ano).then(d => {
        if(d.objects.length === 0) {
          agent.Distribucion.createDistribucion(this.props.currentUser.id, null, this.state.mes, this.state.ano).then(dd => {
            Promise.all([
              agent.Distribucion.recaudaciones(),
              agent.Distribucion.distribucion(this.props.currentUser.id, null, this.state.mes, this.state.ano), // distribucion a entregar en el mes actual
              agent.Distribucion.ventas(this.props.currentUser.id, null, this.state.mes, this.state.ano - 1), // ventas del año anterior mismo mes que el actual
              agent.Distribucion.distribucion(this.props.currentUser.id, null, Number(this.state.mes) === 1? 12: Number(this.state.mes) - 1, Number(this.state.mes) === 1? Number(this.state.ano) - 1: Number(this.state.ano)), // entregadas el mes anterior
              agent.Distribucion.ventas(this.props.currentUser.id, null, Number(this.state.mes) === 1? 12: Number(this.state.mes) - 1, Number(this.state.mes) === 1? Number(this.state.ano) - 1: Number(this.state.ano)), // ventas del mes anterior
              Number(this.state.mes) - 1,
              Number(this.state.ano)
            ]).then(s => {
              this.props.onLoad(s);
              if(!this.props.folios)
                this.props.onLoadFolios(agent.Entrega.getFolios(this.props.entregas));
            });
          });
        }
        else
          Promise.all([ 
            agent.Distribucion.recaudaciones(), 
            d, // distribucion a entregar en el mes actual
            agent.Distribucion.ventas(this.props.currentUser.id, null, this.state.mes, this.state.ano - 1), // ventas del año anterior mismo mes que el actual
            agent.Distribucion.distribucion(this.props.currentUser.id, null, Number(this.state.mes) === 1? 12: Number(this.state.mes) - 1, Number(this.state.mes) === 1? Number(this.state.ano) - 1: Number(this.state.ano)), // entregadas el mes anterior
            agent.Distribucion.ventas(this.props.currentUser.id, null, Number(this.state.mes) === 1? 12: Number(this.state.mes) - 1, Number(this.state.mes) === 1? Number(this.state.ano) - 1: Number(this.state.ano)), // ventas del mes anterior
            Number(this.state.mes) - 1,
            Number(this.state.ano)
          ]).then(s => {
            this.props.onLoad(s);
            console.log('hola mundo x2');
            if(!this.props.folios)
              this.props.onLoadFolios(agent.Entrega.getFolios(this.props.entregas));
          });
      });
    }
  }
  distribucionSugerida = () => {
    this.close();
    window.scrollTo(0, 0);
    this.props.onDistribucionSugerida();
  };

  constructor(props) {
    super(props);
    this.submitForm = recaudaciones => ev => {
      ev.preventDefault();
    };
    this.toggle = p => ev => {
      ev.preventDefault();
      this.props.onToggle(p);
    };
    this.changeInput = (of, rec_id, field) => ev => {
      ev.preventDefault();
      
      let oficialia = of;
      if(field === 'oficialia')
        oficialia.actas_oficialia = ev.target.value;
      else if(field === 'use')
        oficialia.actas_use = ev.target.value;
      else if(field === 'interestatales')
        oficialia.actas_interestatales = ev.target.value;
      
      this.props.onChangeOficialia({ rec_id: rec_id, oficialia: oficialia }/* null*/);
    }
    this.save = () => ev => {
      window.scrollTo(0, 0);
      this.props.onSaveStarted();
      var entregas = [];
      for (var i = 0; i < this.props.recaudaciones.length; i++) {
        let r = this.props.recaudaciones[i];
        for (var j = 0; j < r.oficialias.length; j++) {
          let o = r.oficialias[j];
          if(o.actas_interestatales || o.actas_oficialia || o.actas_use || o.entrega_id)
          {
            if(o.entrega_id)
              entregas.push({
                id: o.entrega_id,
                resource_uri: o.entrega_uri,
                distribucion: this.props.distribucion_uri,
                oficialia: o.resource_uri,
                cve_oficialia: o.cve_oficialia,
                actas_oficialia: o.actas_oficialia === ''? null: String(o.actas_oficialia).replace(/,/g , ""),
                actas_use: o.actas_use === ''? null: String(o.actas_use).replace(/,/g , ""),
                actas_interestatales: o.actas_interestatales === ''? null: String(o.actas_interestatales).replace(/,/g , ""),
                user: '/auth/v1/user/' + this.props.currentUser.id + '/'
              });
            else
              entregas.push({
                distribucion: this.props.distribucion_uri,
                oficialia: o.resource_uri,
                cve_oficialia: o.cve_oficialia,
                actas_oficialia: o.actas_oficialia === ''? null: String(o.actas_oficialia).replace(/,/g , ""),
                actas_use: o.actas_use === ''? null: String(o.actas_use).replace(/,/g , ""),
                actas_interestatales: o.actas_interestatales === ''? null: String(o.actas_interestatales).replace(/,/g , ""),
                user: '/auth/v1/user/' + this.props.currentUser.id + '/'
              });
          }
        }
      }
      if(entregas.length > 0)
        agent.Distribucion.addEntregas({ objects: entregas }).then(() => {
          this.props.onSave(Promise.all([
            agent.Distribucion.recaudaciones(),
            agent.Distribucion.distribucion(this.props.currentUser.id, null, this.props.mes + 1, this.props.ano)
          ]));
        });
      else {
        this.props.onSaveCanceled();
        console.log('nothing to save...');
      }
    }
  }
  componentWillMount() {
    if(!this.props.currentUser)
      window.location.replace('/');
    let cd = new Date();
    agent.Distribucion.distribucion(this.props.currentUser.id, null, cd.getMonth() + 1, cd.getFullYear()).then(d => {
      if(d.objects.length === 0) {
        agent.Distribucion.createDistribucion(this.props.currentUser.id, null, cd.getMonth() + 1, cd.getFullYear())
          .then(dd => {
            Promise.all([
              agent.Distribucion.recaudaciones(),
              agent.Distribucion.distribucion(this.props.currentUser.id, null, cd.getMonth() + 1, cd.getFullYear()), // distribucion a entregar en el mes actual
              agent.Distribucion.ventas(this.props.currentUser.id, null, cd.getMonth() + 1, cd.getFullYear() - 1), // ventas del año anterior mismo mes que el actual
              agent.Distribucion.distribucion(this.props.currentUser.id, null, cd.getMonth() === 0? 12: cd.getMonth(), cd.getMonth() === 0? cd.getFullYear() - 1: cd.getFullYear()), // entregadas el mes anterior
              agent.Distribucion.ventas(this.props.currentUser.id, null, cd.getMonth() === 0? 12: cd.getMonth(), cd.getMonth() === 0? cd.getFullYear() - 1: cd.getFullYear()), // ventas del mes anterior
              cd.getMonth(),
              cd.getFullYear()
            ]).then(s => {
              this.props.onLoad(s);
              if(!this.props.folios)
                this.props.onLoadFolios(agent.Entrega.getFolios(this.props.entregas));
            });
          });
      }
      else
        Promise.all([ 
          agent.Distribucion.recaudaciones(), 
          d, // distribucion a entregar en el mes actual
          agent.Distribucion.ventas(this.props.currentUser.id, null, cd.getMonth() + 1, cd.getFullYear() - 1), // ventas del año anterior mismo mes que el actual
          agent.Distribucion.distribucion(this.props.currentUser.id, null, cd.getMonth() === 0? 12: cd.getMonth(), cd.getMonth() === 0? cd.getFullYear() - 1: cd.getFullYear()), // entregadas el mes anterior
          agent.Distribucion.ventas(this.props.currentUser.id, null, cd.getMonth() === 0? 12: cd.getMonth(), cd.getMonth() === 0? cd.getFullYear() - 1: cd.getFullYear()), // ventas del mes anterior
          cd.getMonth(),
          cd.getFullYear()
        ]).then(s => {
          this.props.onLoad(s);
          if(!this.props.folios)
            this.props.onLoadFolios(agent.Entrega.getFolios(this.props.entregas));
        });
    });
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { open, cambiaMes, mes, ano } = this.state;
    if(!this.props.recaudaciones)
       return (
        <Dimmer active={true} page>
          <Loader size='medium'>Cargando distribución del mes</Loader>
        </Dimmer>
        )
    const recaudaciones = this.props.recaudaciones.map(rec => {
      return {
        id: rec.id,
        title: rec.recaudacion,
        suma: rec.suma,
        active: rec.active,
        content: (
          <div>
          {
            rec.oficialias.map(of => {
              return (<div key={of.id}>
                <span style={{ color:'#465b6b' }}>{of.cve_oficialia} {of.nombre} <strong>{of.suma? '(' + of.suma +')' : '(' + 0 +')' }</strong></span>
                <Form>
                  <Form.Group widths='equal'>
                    <Form.Field>
                      <Input type="text" labelPosition='right' placeholder='Oficialia' value={of.actas_oficialia} onChange={this.changeInput(of, rec.id, 'oficialia')}>
                        <input style={{ width: '40%' }}/>
                        <Popup
                          trigger={<Label style={{ backgroundColor: '#615555', color: 'white', width: '20%', borderTopRightRadius: '0', borderBottomRightRadius: '0' }}>{of.vendidas_oficialia_mes_anterior}</Label>}
                          content="Entregadas en el mes anterior"
                          basic
                        />
                        <Popup
                          trigger={<Label style={{ backgroundColor: '#19847f', color: 'white', width: '20%', borderTopRightRadius: '0', borderBottomRightRadius: '0', borderTopLeftRadius: '0', borderBottomLeftRadius: '0' }}>{of.entregadas_oficialia_mes_anterior}</Label>}
                          content="Vendidas en el mes anterior"
                          basic
                        />
                        <Popup
                          trigger={<Label style={{ backgroundColor: '#346877 ', color: 'white', width: '20%', borderTopLeftRadius: '0', borderBottomLeftRadius: '0' }}>{of.vendidas_oficialia /*año anterior*/ }</Label> }
                          content="Vendidas en el mes mismo mes del año anterior"
                          basic
                        />
                      </Input>
                    </Form.Field>
                    <Form.Field>
                      <Input type="text" labelPosition='right' placeholder='USE' value={of.actas_use} onChange={this.changeInput(of, rec.id, 'use')}>
                        <input style={{ width: '40%' }}/>
                        <Popup
                          trigger={<Label style={{ backgroundColor: '#615555', color: 'white', width: '20%', borderTopRightRadius: '0', borderBottomRightRadius: '0' }}>{of.vendidas_use_mes_anterior}</Label>}
                          content="Entregadas en el mes anterior"
                          basic
                        />
                        <Popup
                          trigger={<Label style={{ backgroundColor: '#19847f', color: 'white', width: '20%', borderTopRightRadius: '0', borderBottomRightRadius: '0', borderTopLeftRadius: '0', borderBottomLeftRadius: '0' }}>{of.entregadas_use_mes_anterior}</Label>}
                          content="Vendidas en el mes anterior"
                          basic
                        />
                        <Popup
                          trigger={<Label style={{ backgroundColor: '#346877 ', color: 'white', width: '20%', borderTopLeftRadius: '0', borderBottomLeftRadius: '0' }}>{of.vendidas_use /*año anterior*/ }</Label>}
                          content="Vendidas en el mes mismo mes del año anterior"
                          basic
                        />
                      </Input>
                    </Form.Field>
                    <Form.Field>
                      <Input type="text" labelPosition='right' placeholder='Interestatales' value={of.actas_interestatales} onChange={this.changeInput(of, rec.id, 'interestatales')}>
                        <input style={{ width: '40%' }}/>
                        <Popup
                          trigger={<Label style={{ backgroundColor: '#615555', color: 'white', width: '20%', borderTopRightRadius: '0', borderBottomRightRadius: '0' }}>{of.vendidas_interestatales_mes_anterior}</Label>}
                          content="Entregadas en el mes anterior"
                          basic
                        />
                        <Popup
                          trigger={<Label style={{ backgroundColor: '#19847f', color: 'white', width: '20%', borderTopRightRadius: '0', borderBottomRightRadius: '0', borderTopLeftRadius: '0', borderBottomLeftRadius: '0' }}>{of.entregadas_interestatales_mes_anterior}</Label>}
                          content="Vendidas en el mes anterior"
                          basic
                        />
                        <Popup
                          trigger={<Label style={{ backgroundColor: '#346877 ', color: 'white', width: '20%', borderTopLeftRadius: '0', borderBottomLeftRadius: '0' }}>{of.vendidas_interestatales /*año anterior*/ }</Label>}
                          content="Vendidas en el mes mismo mes del año anterior"
                          basic
                        />
                      </Input>
                    </Form.Field>
                  </Form.Group>
                </Form>
              </div>)
          })
          }
          </div>
        )
      }
    });
    return (
      <div style={{ marginLeft: '5em', width: '70%', paddingTop: '8em' }}>
        <div style={{ marginTop: '-11em', position: 'fixed', width: '70%', zIndex: 1 }}>
          <div style={{ backgroundColor: 'white', marginBottom: '-1em', height: '4em' }}></div>
          <Segment inverted style={{ width: '100%'}}>
            <Form inverted>
              <Form.Field label={'Distribución regular (' + this.props.meses[Number(this.props.mes)].mes + ' ' + this.props.ano + ')   ' + this.props.total_entrega} />
              <Button color='instagram' content='Contraer todo' icon='compress' labelPosition='left' onClick={this.props.onContract}/>
              <Button color='instagram' content='Expandir todo' icon='expand' labelPosition='left' onClick={this.props.onExpand}/>
              {this.props.mes < (new Date().getMonth()) || this.props.ano < (new Date().getFullYear()) || this.props.folios_asignados? '' : <Button onClick={this.show()} color='youtube' content='Distribución sugerida' icon='warning sign' labelPosition='right' />}
              <Button onClick={this.showCambiaMes()} color='youtube' content='Otro mes' icon='right arrow' labelPosition='right' />
              {this.props.mes < (new Date().getMonth()) || this.props.ano < (new Date().getFullYear()) || this.props.folios_asignados? '' : <Button type='submit' onClick={this.save()}>Guardar</Button>}
            </Form>
          </Segment>
          <div style={{ backgroundColor: 'white', marginTop: '-1em', height: '3em' }}>
            <Form>
              <Form.Group widths='equal'>
                <Form.Field>
                  <Label style={{ color: '#151313', marginTop: '0.4em', width: '100%', textAlign: 'center' }}>Actas oficialia</Label>
                </Form.Field>
                <Form.Field>
                  <Label style={{ color: '#151313', marginTop: '0.4em', width: '100%', textAlign: 'center' }}>Actas use</Label>
                </Form.Field>
                <Form.Field>
                  <Label style={{ color: '#151313', marginTop: '0.4em', width: '100%', textAlign: 'center' }}>Actas interestatales</Label>
                </Form.Field>
              </Form.Group>
            </Form>
          </div>
        </div>
        <Modal dimmer={'blurring'} size={'small'} open={open} onClose={this.close} style={{ height: '20em' }}>
          <Modal.Header>Cargar sugerido</Modal.Header>
          <Modal.Content image style={{ minHeight: '11em' }}>
            <Modal.Description>
              <Header>Alerta</Header>
              <p>Se van a reemplazar todos los datos existentes con la distribución sugerida para este mes</p>
              <p>¿Desea continuar?</p>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button color='black' onClick={this.close}>
              No
            </Button>
            <Button positive icon='checkmark' labelPosition='right' content="Si" onClick={this.distribucionSugerida} />
          </Modal.Actions>
        </Modal>
        <Modal dimmer={'blurring'} size={'small'} open={cambiaMes} onClose={this.closeCambiaMes} style={{ height: '35em' }}>
          <Modal.Header>Cambiar de mes <small><small>(Mes seleccionado {this.props.meses[Number(this.props.mes)].mes} {this.props.ano})</small></small></Modal.Header>
          <Modal.Content image style={{ minHeight: '26em' }}>
            <Modal.Description>
              <p>Mes</p>
              <Select style={{ width: '17em' }} placeholder='Seleccione mes' value={mes} options={this.props.meses.map(m => { return { key: m.id, value: m.id, text: m.mes } })} onChange={this.changeMes()}/>
              <p style={{ marginTop: '1em' }}>Año</p>
              <Input type="number" style={{ width:'17em' }} placeholder='Año' value={ano} onChange={this.changeAno()} />
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button color='black' onClick={this.closeCambiaMes}>
              Cancelar
            </Button>
            <Button positive onClick={this.cambiaFecha}>
              Aceptar
            </Button>
          </Modal.Actions>
        </Modal>
        <Dimmer active={this.props.inProgress} page>
          <Loader size='medium'></Loader>
        </Dimmer>
        <div style={{ marginTop: '2.1em', marginBottom: '9em' }}>
        {
          recaudaciones.map(p => 
            <Accordion key={p.id}>
              <Accordion.Title onClick={this.toggle(p)}>
                <Icon name={p.active===0? 'caret down': 'caret right'} />
                  {p.title} {'(' + p.suma + ')'}
              </Accordion.Title>
              <Accordion.Content active={p.active===0}>
                {p.content}
              </Accordion.Content>
            </Accordion>
          )
        }
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(DistribucionRegular);
