import ListErrors from './ListErrors';
import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { Button, Modal, Header, Icon, Label, Form, Dimmer, Loader } from 'semantic-ui-react'
import {
  UPDATE_FIELD_AUTH,
  LOGIN,
  LOGIN_PAGE_UNLOADED,
  SET_ACTIVE_MENU
} from '../constants/actionTypes';

const mapStateToProps = state => ({ 
  ...state.auth, 
  currentUser: state.common.currentUser
});

const mapDispatchToProps = dispatch => ({
  onChangeEmail: value =>
    dispatch({ type: UPDATE_FIELD_AUTH, key: 'email', value }),
  onChangePassword: value =>
    dispatch({ type: UPDATE_FIELD_AUTH, key: 'password', value }),
  onSubmit: payload =>
    dispatch({ type: LOGIN, payload}),
  onUnload: () =>
    dispatch({ type: LOGIN_PAGE_UNLOADED }),
  onSuccess: () => dispatch({ type: SET_ACTIVE_MENU, menu: 'lotes' }),
});

class Login extends React.Component {
  state = { alert: false, loading: false }
  constructor() {
    super();
    this.changeEmail = ev => this.props.onChangeEmail(ev.target.value);
    this.changePassword = ev => this.props.onChangePassword(ev.target.value);
    this.submitForm = (email, password) => ev => {
      ev.preventDefault();
      this.setState({ loading: true });
      agent.Auth.login(email, password).then(r => {
        if(r.error)
          this.setState({ alert: true, modalContent: r.error, loading: false });
        else {
          this.props.onSubmit(r);
          this.setState({ loading: false });
          this.props.onSuccess();
          //window.location.replace('#/lotes');
        }
      })
    };
    console.log(this.props)

  }
  componentWillUnmount() {
    this.props.onUnload();
    //window.location.replace('#/lotes');
  }

  render() {
    const { email, password, inProgress } = this.props;
    const { alert, modalContent } = this.state;

    return (
      <div>
        <Dimmer active={inProgress} page>
          <Loader size='medium'>Espere...</Loader>
        </Dimmer>
        <Modal open={alert} onClose={this.handleClose} basic size='small'>
          <Header icon='browser' content='Error' />
          <Modal.Content>
            <h3 style={{ color: '#e62525' }}>{modalContent}</h3>
          </Modal.Content>
          <Modal.Actions>
            <Button color='green' onClick={() => this.setState({alert: false})} inverted>
              <Icon name='checkmark' />Ok
            </Button>
          </Modal.Actions>
        </Modal>
        <div style={{bottom: '0', left: '0', position: 'fixed', right: '0', top: '0'}}>
          <div style={{margin: '10em auto', minHeight: '7em', width: '400px'}}>
            <Label as='a' color='teal' ribbon>Iniciar sesión</Label>
            <ListErrors errors={this.props.errors} />
            <Form style={{ marginTop: '3em' }} onSubmit={this.submitForm(email, password)}>
              <Form.Field>
                <label style={{ color: '#525252' }}>Usuario</label>
                <input type="text" placeholder="Usuario" value={email} onChange={this.changeEmail} autoComplete="username"/>
              </Form.Field>
              <Form.Field>
                <label style={{ color: '#525252' }}>Contraseña</label>
                <input type="password" placeholder="Contraseña" value={password} onChange={this.changePassword} autoComplete="current-password"/>
              </Form.Field>
              <Button type="submit" color="teal" disabled={!(email && email.length > 0 && password && password.length > 0)}>Entrar</Button>
            </Form>
          </div>
        </div>
      </div>
      );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
