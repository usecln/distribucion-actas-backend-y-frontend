import React from 'react';

const Banner = ({ appName, token }) => {
  if (token)
    return null;
  return (
    <div className="banner" style={{ marginTop: '9em', width: '70%', marginLeft: '5em' }}>
      <div className="container" >
        <h1 className="logo-font">
          {appName.toLowerCase()}
        </h1>
        <p>Herramienta para administrar y distribuir lotes de actas a las oficialias del estado de Sinaloa</p>
      </div>
    </div>
  );
};

export default Banner;
