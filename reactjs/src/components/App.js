import agent from '../agent';
import Header from './Header';
import Sidebar from './Sidebar';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { 
  APP_LOAD, 
  EE_PAGE_INIT,
  REDIRECT 
} from '../constants/actionTypes';
import { Dimmer, Loader } from 'semantic-ui-react'
import '../css/sidebar.css';

const mapStateToProps = state => ({
  appLoaded: state.common.appLoaded,
  appName: state.common.appName,
  currentUser: state.common.currentUser,
  redirectTo: state.common.redirectTo
});

const mapDispatchToProps = dispatch => ({
  onLoad: (payload, token) => dispatch({ type: APP_LOAD, payload, token, skipTracking: true }),
  init: payload => dispatch({ type: EE_PAGE_INIT, payload }),
  onRedirect: () =>
    dispatch({ type: REDIRECT })
});

class App extends React.Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.redirectTo) {
      this.context.router.replace(nextProps.redirectTo);
      this.props.onRedirect();
    }
  }

  componentWillMount() {
    const token = window.localStorage.getItem('jwt');
    if (token) {
      agent.setToken(token);

    }
    this.props.onLoad(token ? agent.Auth.current(): null, token);
    this.props.init(token ? agent.Distribucion.recaudaciones(): null, []);
  }

  state = { activeItem: '' }



  render() {
    const { appLoaded, children } = this.props;
      return (
        <div id="main">
          <Dimmer active={!appLoaded} page>
            <Loader size='medium'>Espere...</Loader>
          </Dimmer>
          <Sidebar></Sidebar>
          <Header></Header>
          {children}
        </div>
      );
  }
}

App.contextTypes = {
  router: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(App);