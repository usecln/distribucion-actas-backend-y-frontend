import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { Menu, Button} from 'semantic-ui-react'

import '../css/main.css';

const LoggedOutView = currentUser => {
  if (!currentUser) {
    return null;
  }
  return null;
};

const LoggedInView = data => {
  if (data.currentUser) {
    return (
      <Menu size='large' className="banner">
        <Menu.Menu position='right'>
          <Menu.Item name='Salir' className="opcion-nav">
            <Button basic color='teal' as={Link} to="salir">
              Salir
            </Button>
          </Menu.Item>
        </Menu.Menu>
      </Menu>
    );
  } else {
    return null;
  }
};

const mapStateToProps = state => ({
  currentUser: state.common.currentUser
});

class Header extends React.Component {
  render() {
    const { currentUser } = this.props;
    return (
      <div >
        <LoggedOutView currentUser={currentUser} />
        <LoggedInView currentUser={currentUser} />
      </div>
    );
  }
}

export default connect(mapStateToProps, {})(Header);