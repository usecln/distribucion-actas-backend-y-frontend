import React from 'react';
import agent from '../agent';
import { connect } from 'react-redux';
import { Table, Form, Button, Segment, Header, Modal, Dimmer, Loader, Select, Input } from 'semantic-ui-react'

import {
  ER_PAGE_LOADED,
  ER_PAGE_UNLOADED,
  ER_GUARDAR,
  ER_PAGE_LOAD_FOLIOS,
  UPDATE_FIELD_OFICIALIA,
  ASYNC_START,
  ASYNC_END
} from '../constants/actionTypes';

const mapStateToProps = state => ({
  ...state.entrega,
  currentUser: state.common.currentUser,
  inProgress: state.settings.inProgress
});

const mapDispatchToProps = dispatch => ({
  onLoad: payload => dispatch({ type: ER_PAGE_LOADED, payload }),
  onLoadFolios: payload => dispatch({ type: ER_PAGE_LOAD_FOLIOS, payload }),
  onChangeOficialia: oficialia => dispatch({ type: UPDATE_FIELD_OFICIALIA, oficialia }),
  onSaveStarted: () => dispatch({ type: ASYNC_START }),
  onSaveCanceled: () => dispatch({ type: ASYNC_END }),
  onSave: payload => dispatch({ type: ER_GUARDAR, payload }),
  onUnload: () => dispatch({ type: ER_PAGE_UNLOADED })
});

class EntregaRegular extends React.Component {
  state = { open: false, cambiaMes: false, ano: (new Date().getFullYear()), mes: (new Date().getMonth() + 1) }

  show = () => () => this.setState({ open: true })
  showCambiaMes = () => () => this.setState({ mes: (Number(this.props.mes) + 1), ano: this.props.ano, cambiaMes: true })
  close = () => this.setState({ open: false })
  closeCambiaMes = () => this.setState({ cambiaMes: false })
  changeAno = () => e => {
    if(Number(e.target.value) < 3000)
      this.setState({ ano: e.target.value });
  }
  changeMes = y => e => {
    let m = this.props.meses.filter(m => m.mes === e.target.textContent);
    if(m.length > 0)
      this.setState({ mes: m[0].id });
  }
  cambiaFecha = () => {
    if(Number(this.state.ano) < 1980 || Number(this.state.ano) > 2500)
      alert("El campo de año solo permite valores entre 1980 y 2500");
    else
    {
      this.closeCambiaMes();
      this.props.onSaveStarted();
      Promise.all([
        agent.Distribucion.recaudaciones(),
        agent.Distribucion.distribucion(this.props.currentUser.id, null, Number(this.state.mes), Number(this.state.ano)),
        Number(this.state.mes) - 1,
        Number(this.state.ano)
      ]).then(s => {
        this.props.onLoad(s);
        if(!this.props.folios)
          this.props.onLoadFolios(agent.Entrega.getFolios(this.props.entregas));
      });
    }
  }

  asignaFolios = () => {
    this.close();
    window.scrollTo(0, 0);
    this.props.onSaveStarted();
    agent.Entrega.asignaFolios(this.props.entregas, this.props.currentUser.username).then(f => this.props.onLoadFolios(agent.Entrega.getFolios(this.props.entregas)));
  };
  downloadPDF = () => () => {
    var link = document.createElement("a");
    link.download = "distribucion.xlsx";
    link.href = agent.API_ROOT + '/api/v1/get_pdf/?distribucion=' + this.props.distribucion_id;
    link.click();
  };

  constructor(props) {
    super(props);
    this.toggle = p => ev => {
      ev.preventDefault();
      this.props.onToggle(p);
    };
    this.changeInput = (of, rec_id, field) => ev => {
      ev.preventDefault();
      
      let oficialia = of;
      if(field === 'oficialia')
        oficialia.actas_oficialia = ev.target.value;
      else if(field === 'use')
        oficialia.actas_use = ev.target.value;
      else if(field === 'interestatales')
        oficialia.actas_interestatales = ev.target.value;
      
      this.props.onChangeOficialia({ rec_id: rec_id, oficialia: oficialia }/* null*/);
    }
    this.save = () => ev => {
      window.scrollTo(0, 0);
      this.props.onSaveStarted();
      var entregas = [];
      for (var i = 0; i < this.props.recaudaciones.length; i++) {
        let r = this.props.recaudaciones[i];
        for (var j = 0; j < r.oficialias.length; j++) {
          let o = r.oficialias[j];
          if(o.actas_interestatales || o.actas_oficialia || o.actas_use || o.entrega_id)
          {
            if(o.entrega_id)
              entregas.push({
                id: o.entrega_id,
                resource_uri: o.entrega_uri,
                distribucion: this.props.distribucion_uri,
                oficialia: o.resource_uri,
                cve_oficialia: o.cve_oficialia,
                actas_oficialia: o.actas_oficialia === ''? null: String(o.actas_oficialia).replace(/,/g , ""),
                actas_use: o.actas_use === ''? null: String(o.actas_use).replace(/,/g , ""),
                actas_interestatales: o.actas_interestatales === ''? null: String(o.actas_interestatales).replace(/,/g , ""),
                user: '/auth/v1/user/' + this.props.currentUser.id + '/'
              });
            else
              entregas.push({
                distribucion: this.props.distribucion_uri,
                oficialia: o.resource_uri,
                cve_oficialia: o.cve_oficialia,
                actas_oficialia: o.actas_oficialia === ''? null: String(o.actas_oficialia).replace(/,/g , ""),
                actas_use: o.actas_use === ''? null: String(o.actas_use).replace(/,/g , ""),
                actas_interestatales: o.actas_interestatales === ''? null: String(o.actas_interestatales).replace(/,/g , ""),
                user: '/auth/v1/user/' + this.props.currentUser.id + '/'
              });
          }
        }
      }
      if(entregas.length > 0)
        agent.Distribucion.addEntregas({ objects: entregas }).then(() => {
          this.props.onSave(Promise.all([
            agent.Distribucion.recaudaciones(),
            agent.Distribucion.distribucion(this.props.currentUser.id, null, this.props.mes + 1, this.props.ano)
          ]));
        });
      else {
        this.props.onSaveCanceled();
        console.log('nothing to save...');
      }
    }
  }
  componentWillMount() {
    let cd = new Date();
    if(!this.props.currentUser)
      window.location.replace('/');
    
    Promise.all([
      agent.Distribucion.recaudaciones(),
      agent.Distribucion.distribucion(this.props.currentUser.id, null, cd.getMonth() + 1, cd.getFullYear()),
      cd.getMonth(),
      cd.getFullYear(),
    ]).then(s => {
      this.props.onLoad(s);
      if(!this.props.folios)
        this.props.onLoadFolios(agent.Entrega.getFolios(this.props.entregas));
    });
  }

  componentWillUnmount() {
    this.props.onUnload();
  }

  render() {
    const { open, cambiaMes, mes, ano } = this.state;

    if(!this.props.oficialias)
       return (
        <Dimmer active={true} page>
          <Loader size='medium'>Cargando distribución del mes</Loader>
        </Dimmer>)
    return (
      <div style={{ marginLeft: '5em', width: '70%', paddingTop: '8em' }}>
        <div style={{ marginTop: '-11em', position: 'fixed', width: '70%', zIndex: 1 }}>
          <div style={{ backgroundColor: 'white', marginBottom: '-1em', height: '4em' }}></div>
          <Segment inverted style={{ width: '100%'}}>
            <Form inverted>
              <Form.Field label={'Distribución regular (' + this.props.meses[Number(this.props.mes)].mes + ' ' + this.props.ano + ') ' + this.props.total} />
              {!this.props.folios_asignados && (Number(this.props.mes) === new Date().getMonth() || Number(this.props.mes) === new Date().getMonth() + 1) && Number(this.props.ano) === (new Date().getFullYear())? <Button onClick={this.show()} color='youtube' content='Asignar folios' icon='warning sign' labelPosition='right' /> : ''}
              <Button onClick={this.showCambiaMes()} color='youtube' content='Cambiar mes' icon='right arrow' labelPosition='right' />
              {!this.props.folios_asignados? <Button color='instagram' disabled>Descargar EXCEL</Button> : <Button color='instagram' onClick={this.downloadPDF()}>Descargar EXCEL</Button> }
            </Form>
          </Segment>
          <div style={{ backgroundColor: 'white', marginTop: '-1em', height: '3em' }}></div>
          <Table striped style={{marginTop: '-1em'}}>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell style={{ textAlign: 'left', width: '25%' }}>Oficialia</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'right', width: '15%' }}>Actas oficialia</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'right', width: '15%' }}>Actas use</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'right', width: '15%' }}>Actas interestatales</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'center', width: '15%' }}>Folio inicial</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'center', width: '15%' }}>Folio final</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
          </Table>
        </div>
        <Modal dimmer={'blurring'} size={'small'} open={open} onClose={this.close} style={{ height: '20em' }}>
          <Modal.Header>Asignar folios</Modal.Header>
          <Modal.Content image style={{ minHeight: '11em' }}>
            <Modal.Description>
              <Header>Alerta</Header>
              <p>Una vez asignados los folios ya no se podran hacer cambios en la distribución del mes</p>
              <p>¿Desea continuar?</p>
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button color='black' onClick={this.close}>
              No
            </Button>
            <Button positive icon='checkmark' labelPosition='right' content="Si" onClick={this.asignaFolios} />
          </Modal.Actions>
        </Modal>
        <Modal dimmer={'blurring'} size={'small'} open={cambiaMes} onClose={this.closeCambiaMes} style={{ height: '35em' }}>
          <Modal.Header>Cambiar de mes <small><small>(Mes seleccionado {this.props.meses[Number(this.props.mes)].mes} {this.props.ano})</small></small></Modal.Header>
          <Modal.Content image style={{ minHeight: '26em' }}>
            <Modal.Description>
              <p>Mes</p>
              <Select style={{ width: '17em' }} placeholder='Seleccione mes' value={mes} options={this.props.meses.map(m => { return { key: m.id, value: m.id, text: m.mes } })} onChange={this.changeMes()}/>
              <p style={{ marginTop: '1em' }}>Año</p>
              <Input type="number" style={{ width:'17em' }} placeholder='Año' value={ano} onChange={this.changeAno()} />
            </Modal.Description>
          </Modal.Content>
          <Modal.Actions>
            <Button color='black' onClick={this.closeCambiaMes}>
              Cancelar
            </Button>
            <Button positive onClick={this.cambiaFecha}>
              Aceptar
            </Button>
          </Modal.Actions>
        </Modal>
        <Dimmer active={this.props.inProgress} page>
          <Loader size='medium'></Loader>
        </Dimmer>
        <div style={{ marginTop: '2em', marginBottom: '9em' }}>
          <Table striped>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell style={{ textAlign: 'left', width: '25%' }}>Oficialia</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'right', width: '15%' }}>Actas oficialia</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'right', width: '15%' }}>Actas use</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'right', width: '15%' }}>Actas interestatales</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'center', width: '15%' }}>Folio inicial</Table.HeaderCell>
                <Table.HeaderCell style={{ textAlign: 'center', width: '15%' }}>Folio final</Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {
                this.props.oficialias.map(o => 
                  (
                    <Table.Row key={o.id}>
                      <Table.Cell style={{ textAlign: 'left', width: '25%' }}>{(o.cve_oficialia? o.cve_oficialia + ' ': '') + o.nombre + ' (' + o.suma + ')'}</Table.Cell>
                      <Table.Cell style={{ textAlign: 'right', width: '15%' }}>{o.actas_oficialia}</Table.Cell>
                      <Table.Cell style={{ textAlign: 'right', width: '15%' }}>{o.actas_use}</Table.Cell>
                      <Table.Cell style={{ textAlign: 'right', width: '15%' }}>{o.actas_interestatales}</Table.Cell>
                      <Table.Cell style={{ textAlign: 'center', width: '15%' }}>
                        { o.folio_inicial? 'A25 ' + o.folio_inicial: '' }
                        <span style={{ color: 'green' }}>{ o.folio_inicial1? 'A25 ' + o.folio_inicial1: '' }</span><br />
                        <span style={{ color: 'red' }}>{ o.folio_final2? 'A25 ' + o.folio_inicial2: ''}</span>
                        </Table.Cell>
                      <Table.Cell style={{ textAlign: 'center', width: '15%' }}>
                        { o.folio_final? 'A25 ' + o.folio_final: '' }
                        <span style={{ color: 'green' }}>{ o.folio_inicial1? 'A25 ' + o.folio_final1: ''}</span><br />
                        <span style={{ color: 'red' }}>{ o.folio_final2? 'A25 ' + o.folio_final2: ''}</span>
                      </Table.Cell>
                    </Table.Row>
                  )
                )
              }
            </Table.Body>
          </Table>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(EntregaRegular);