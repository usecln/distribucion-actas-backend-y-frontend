import { EE_PAGE_LOADED, EE_PAGE_INIT, EE_PAGE_UNLOADED, EE_PAGE_LOAD_TIPO_LOTES, EE_PAGE_LOAD_FOLIOS, EE_PAGE_ACTUALIZA_ENTREGAS } from '../constants/actionTypes';
function formatMoney (val) {
  var c = 0, d = ".", t = ",", s = (val || 0) < 0 ? "-" : "", 

  i = String(parseInt(Math.abs(val || 0).toFixed(c), 10)), 

  j = i.length;
  j = j > 3 ? j % 3 : 0;
  return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(val - i).toFixed(c).slice(2) : "");
}
export default (state = {}, action) => {

  switch (action.type) {
    case EE_PAGE_INIT:
      let of = [], oficialias = [], oficialias_full_list = [];
      if(action.payload)
      {
        console.log(action.payload)
        if(action.payload.objects) {
          for(let r of action.payload.objects) {
            for(let o of r.oficialias)
              of.push(o);
          }
        }
        else {
          for(let r of action.payload) {
            for(let o of r.oficialias)
              of.push(o);
          }
        }
        oficialias_full_list =  JSON.parse(JSON.stringify(of));
        
        of = of.sort((a,b) => Number(a.cve_oficialia) - Number(b.cve_oficialia));
        of = of.map(o => { return { key: o.id, value: o.id, text: (Number(o.cve_oficialia) === 0? '': o.cve_oficialia + ' ' ) + o.nombre } });
      }
      
      return {
        ...state,
        tipo_lotes: [],
        lista_oficialias: of,
        oficialias_full_list,
        meses: [
          {
            id: 1,
            mes:'Enero'
          },{
            id: 2, 
            mes:'Febrero'
          },{
            id: 3,
            mes: 'Marzo'
          },{
            id: 4,
            mes: 'Abril'
          },{
            id: 5, 
            mes: 'Mayo'
          },{
            id: 6,
            mes: 'Junio'
          },{
            id: 7,
            mes: 'Julio'
          },{
            id: 8,
            mes: 'Agosto'
          },{
            id: 9,
            mes: 'Septiembre'
          },{
            id: 10, 
            mes: 'Octubre'
          },{
            id: 11,
            mes: 'Noviembre'
          },{
            id: 12, 
            mes: 'Diciembre'
          }
        ]
      }
    case EE_PAGE_LOADED:
      let cd = new Date();
      return {
        ...state,
        visitador: action.payload? true: false,
        lista_oficialias: action.payload? state && state.lista_oficialias && state.lista_oficialias.filter(xx => !isNaN( Number(xx.text.charAt(0)) )): state.lista_oficialias,
        total: 0,
        dia: cd.getDate(),
        mes: cd.getMonth(),
        ano: cd.getFullYear()
      }
    case EE_PAGE_UNLOADED:
      return state;
    case EE_PAGE_LOAD_FOLIOS:
      oficialias_full_list = JSON.parse(JSON.stringify((state.oficialias_full_list || [])));
      oficialias = state.oficialias.map(o => { 
        let entregas = action.payload.filter(entrega => entrega.entrega === o.id);

        if(entregas && entregas.length === 1) {
          if(Number(entregas[0].folio_final) !== entregas[0].folio_inicial-1) {
            o.folio_inicial = formatMoney(Number(entregas[0].folio_inicial));
            o.folio_final = formatMoney(Number(entregas[0].folio_final));
          }
        }
        else if(entregas && entregas.length > 1)
        {
          o.multiple_lotes = true;
          o.folio_inicial1 = formatMoney(Number(entregas[0].folio_inicial));
          o.folio_final1 = formatMoney(Number(entregas[0].folio_final));
          o.folio_inicial2 = formatMoney(Number(entregas[entregas.length - 1].folio_inicial));
          o.folio_final2 = formatMoney(Number(entregas[entregas.length - 1].folio_final));
        }

        return o;
      });
      return {
        ...state,
        oficialias
      }
    case EE_PAGE_LOAD_TIPO_LOTES: 
      console.log(action.payload);
     return {
       ...state,
       tipo_lotes: action.payload? action.payload.map(o => { return { key: o.id, value: o.id, text: o.tipo } }): []
     }
    case EE_PAGE_ACTUALIZA_ENTREGAS:
      oficialias = []; 
      let entregas = [], distribucion_id = null;
      oficialias_full_list = JSON.parse(JSON.stringify( (state.oficialias_full_list || []) ));
      entregas = (action.payload && action.payload[0] && action.payload[0].objects && action.payload[0].objects[0] && action.payload[0].objects[0].entregas) || [];
      //console.log(entregas);

      distribucion_id = action.payload && action.payload[0] && action.payload[0].objects && action.payload[0].objects[0] && action.payload[0].objects[0].id;
      //lista_oficialias = oficialias_full_list.filter(o => oficialias.filter(o2 => Number(o2.id) === Number(o.key)).length === 0);

      var folios_disponibles = 0;
      if(action.payload[4] && action.payload[4].objects)
      {
        action.payload[4].objects.map(l => {
          if(Number(l.ultimo_folio) === -1)
            folios_disponibles += Number(l.folio_final) - Number(l.folio_inicial) + 1;
          else
            folios_disponibles += Number(l.folio_final) - Number(l.ultimo_folio);
          return l;
        })
      }
      oficialias = [];
      let total = 0;

      for(let en of entregas) {
        
        total += en.suma;
        let o = oficialias_full_list.find(o => en.oficialia === o.resource_uri); 
        
        if(o) {
          //let en = entregas.filter(e => e.oficialia === o.resource_uri);
          //en = en[0] || {};

          oficialias.push({
            id: en.id,
            cve_oficialia: o.cve_oficialia,
            resource_uri: o.resource_uri,
            nombre: o.nombre,
            act_oficialia: o.act_oficialia,
            act_use: o.act_use,
            act_interestatales: o.act_interestatales,
            actas_oficialia: formatMoney(en.actas_oficialia),
            actas_use: formatMoney(en.actas_use),
            actas_interestatales: formatMoney(en.actas_interestatales),
            suma: formatMoney(en.suma),
            responsable: (o.responsable && o.responsable.responsable) || '',
            entrega_id: en.id,
            tipo_lote: en.tipo.tipo,
            tipo_id: en.tipo.id
          });
        }
      }

      //entregas = entregas.map(e => { return {...e, suma: Number(e.actas_oficialia) + Number(e.actas_use) + Number(e.actas_interestatales)} });
      
      console.log('entregas', entregas)
      /*
      oficialias = oficialias.map(o => {
        let en = entregas.filter(e => e.oficialia === o.resource_uri);
        en = en[0] || {};
        total += en.suma;
        return {
          id: en.id,
          cve_oficialia: o.cve_oficialia,
          resource_uri: o.resource_uri,
          nombre: o.nombre,
          act_oficialia: o.act_oficialia,
          act_use: o.act_use,
          act_interestatales: o.act_interestatales,
          actas_oficialia: formatMoney(en.actas_oficialia),
          actas_use: formatMoney(en.actas_use),
          actas_interestatales: formatMoney(en.actas_interestatales),
          suma: formatMoney(en.suma),
          entrega_id: en.id,
          tipo_lote: en.tipo.tipo,
          tipo_id: en.tipo.id
        }*/
      return {
        ...state,
        distribucion_id,
        dia: action.payload[1],
        mes: action.payload[2],
        ano: action.payload[3],
        disponibles: folios_disponibles,
        folios_disponibles: formatMoney(folios_disponibles),
        oficialias,
        total: formatMoney(total),
        entregas: entregas.map(e => e.id),
        ready: true
      }
    default:
      return state;
  }
};