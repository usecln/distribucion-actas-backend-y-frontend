import { LOTES_PAGE_LOADED, LOTES_PAGE_UNLOADED, REFRESH_LOTES, EE_PAGE_ACTUALIZA_TIPO_FOLIO } from '../constants/actionTypes';

export default (state = {}, action) => {
  function formatMoney(n) {
    var c = 0, d = ".", t = ",", s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c), 10) ), 
    j = i.length;
    j = j > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };
  switch (action.type) {
    case LOTES_PAGE_LOADED:
    case REFRESH_LOTES:
      const st = {
        ...state,
        folios_disponibles_por_tipo: '0',
        lotes: action && action.payload && action.payload.length > 0 && action.payload[0].objects && action.payload[0].objects.length > 0? action.payload[0].objects.map(l=> {
          l.ta = Number(l.folio_final) - Number(l.folio_inicial) + 1;
          l.td = Number(l.ultimo_folio) === -1? l.ta : Number(l.ta) - (Number(l.ultimo_folio) - Number(l.folio_inicial) + 1);
          l.total_actas = formatMoney(l.ta);
          l.progress = Number(100 / ((Number(l.folio_final) - Number(l.folio_inicial) + 1)) * l.td, 10).toFixed(2);
          l.disponibles = formatMoney(l.td);
          l.folio_inicial = formatMoney(l.folio_inicial);
          l.folio_final = formatMoney(l.folio_final);
          l.ultimo_folio = formatMoney(l.ultimo_folio);
          return l;
        }) : []
      };
      st.folios_disponibles_por_tipo = formatMoney(st.lotes.filter(lo => Number(lo.tipo.id) === Number(state.tipo_lote)).reduce((total, lote) => total += lote.td, 0));
      st.td = formatMoney(st.lotes.reduce((total, lote) => total += lote.td, 0));
      st.ta = formatMoney(st.lotes.reduce((total, lote) => total += lote.ta, 0));
      return st;
    case LOTES_PAGE_UNLOADED:
      return state;
    case EE_PAGE_ACTUALIZA_TIPO_FOLIO:
      return {
        ...state,
        tipo_lote: action.payload,
        folios_disponibles_por_tipo: formatMoney(state.lotes.filter(l => Number(l.tipo.id) === Number(action.payload)).reduce((total, lote) => total += lote.td, 0))
      }
    default:
      return state;
  }
};