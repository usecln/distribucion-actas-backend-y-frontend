import {
  LOGIN,
  REGISTER,
  LOGIN_PAGE_UNLOADED,
  REGISTER_PAGE_UNLOADED,
  ASYNC_START,
  ASYNC_END,
  UPDATE_FIELD_AUTH,
  EE_PAGE_INIT
} from '../constants/actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case LOGIN:
    case REGISTER:
      return {
        ...state,
        inProgress: true,
        errors: action.payload ? action.payload.errors : null
      };
    case LOGIN_PAGE_UNLOADED:
    case REGISTER_PAGE_UNLOADED:
      return {};
    case ASYNC_START:
      return {
        ...state, 
        inProgress: true
      };
    case ASYNC_END:
      return {
        ...state,
        inProgress: false
      };
    case UPDATE_FIELD_AUTH:
      return {
        ...state,
        [action.key]: action.value
      };
    case EE_PAGE_INIT:
      console.log('logeado?');
      return state;
    default:
      return state;
  }
};