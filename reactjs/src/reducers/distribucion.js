import {
  DR_PAGE_LOADED,
  DR_PAGE_UNLOADED,
  UPDATE_FIELD_OFICIALIA,
  DR_CONTRACT_FORMS,
  DR_EXPAND_FORMS,
  DR_TOGGLE_FORM,
  DR_DISTRIBUCION_SUGERIDA,
  DR_PAGE_LOAD_FOLIOS,
  DR_GUARDAR
} from '../constants/actionTypes';

export default (state = {}, action) => {
  function formatMoney(n) {
    var c = 0, d = ".", t = ",", s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c), 10) ), 
    j = i.length;
    j = j > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };
  function getActas(ventas, ofi, usee, inte) {
    if(!ventas)
      return [0,0,0];
    let oficialia = ventas.filter(o => Number(o.cve_oficialia) === ofi);
    let insterestatales = ventas.filter(o => Number(o.cve_oficialia) === inte);
    let use = ventas.filter(o => Number(o.cve_oficialia) === usee);
    let actas_oficialia = '0', 
      actas_interestatales = '0', 
      actas_use = '0';

    if(oficialia.length === 1 && oficialia[0].actas_vendidas) 
      actas_oficialia = formatMoney(parseInt( Number(oficialia[0].actas_vendidas), 10 ));

    if(insterestatales.length === 1 && insterestatales[0].actas_vendidas) 
      actas_interestatales = formatMoney(parseInt( Number(insterestatales[0].actas_vendidas), 10 ));

    if(Number(use.length) === 1 && use[0].actas_vendidas) 
      actas_use = formatMoney(parseInt( Number(use[0].actas_vendidas), 10 ));

    return [actas_oficialia, actas_use, actas_interestatales];
  }
  function getDistribucion(o, ventas) {
    if(o && o.cve_oficialia) {
      let actas = getActas(ventas, 
        isNaN(Number(o.act_oficialia))? null: Number(o.act_oficialia), 
        isNaN(Number(o.act_use))? null: Number(o.act_use), 
        isNaN(Number(o.act_interestatales))? null: Number(o.act_interestatales));
      o.actas_oficialia = actas[0];
      o.actas_use = actas[1];
      o.actas_interestatales = actas[2];
      return o;
    }
    else 
      return { id: o.id, actas_oficialia: 0, actas_use: 0, actas_interestatales: 0 };
  }
  switch (action.type) {
    case DR_PAGE_LOADED:
      var recaudaciones = null;
      var distribucion_id = null, distribucion_uri = null, total_entrega = 0, entregas = [];
      var ventas = action.payload[2];
      var ventas_mes_anterior = action.payload[4];
      var data = action.payload[1].objects;
      var entregadas_mes_anterior = action.payload[3].objects[0];
      if(action.payload && action.payload.length > 0) {
        recaudaciones = action.payload[0].objects && action.payload[0].objects.length > 0
        ? action.payload[0].objects.map(r => {
          r.active = 0;
          r.suma = 0;
          //let 
          r.oficialias = r.oficialias.map(o => {
            let e = null, f = null;
            if(data[0])
              e = data[0].entregas.filter(ent => ent.oficialia === o.resource_uri);
            if(entregadas_mes_anterior)
              f = entregadas_mes_anterior.entregas.filter(ent => ent.oficialia === o.resource_uri);
            let actas = getDistribucion(o, ventas);

            o.vendidas_oficialia = actas.actas_oficialia;
            o.vendidas_use = actas.actas_use;
            o.vendidas_interestatales = actas.actas_interestatales;

            let actas_mes_anterior = getDistribucion(o, ventas_mes_anterior);
            o.entregadas_oficialia_mes_anterior = actas_mes_anterior.actas_oficialia;
            o.entregadas_use_mes_anterior = actas_mes_anterior.actas_use;
            o.entregadas_interestatales_mes_anterior = actas_mes_anterior.actas_interestatales;

            o.actas_oficialia = 0;
            o.actas_interestatales = 0;
            o.actas_use = 0;

            if(e && e.length > 0) {
              let entrega = e[0];
              o.suma = Number(entrega.actas_oficialia) + Number(entrega.actas_use) + Number(entrega.actas_interestatales);
              o.actas_interestatales = formatMoney(Number(entrega.actas_interestatales));
              o.actas_use = formatMoney(Number(entrega.actas_use));
              o.actas_oficialia = formatMoney(Number(entrega.actas_oficialia));

              r.suma += o.suma;
              o.suma = formatMoney(o.suma);
              o.entrega_id = entrega.id;
              o.entrega_uri = entrega.resource_uri;
            }
            else {
              o.actas_interestatales = 0;
              o.actas_use = 0;
              o.actas_oficialia = 0;
            }
            
            if(f && f.length > 0) {
              let venta_mes_anterior = f[0];
              o.vendidas_oficialia_mes_anterior = formatMoney(Number(venta_mes_anterior.actas_oficialia));
              o.vendidas_use_mes_anterior = formatMoney(Number(venta_mes_anterior.actas_use));
              o.vendidas_interestatales_mes_anterior = formatMoney(Number(venta_mes_anterior.actas_interestatales));
            }
            else {
              o.vendidas_oficialia_mes_anterior = 0;
              o.vendidas_use_mes_anterior = 0;
              o.vendidas_interestatales_mes_anterior = 0;
            }
            if(o.entrega_id)
              entregas.push(o.entrega_id);
            return o;
          });
          total_entrega += r.suma;
          r.suma = formatMoney(r.suma);
          return r;
        }): null;

        if(data && data.length > 0 && !distribucion_id) {
          distribucion_id = data[0].id;
          distribucion_uri = data[0].resource_uri;
        }
      }
  
      return {
        ...state,
        folios_asignados: false,
        recaudaciones: recaudaciones,
        total_entrega: formatMoney(total_entrega),
        ventas: ventas,
        distribucion_id: distribucion_id,
        distribucion_uri: distribucion_uri,
        mes: action.payload[5],
        ano: action.payload[6],
        entregas: entregas,
        meses: [
          {
            id:1,
            mes:'Enero'
          },{
            id: 2, 
            mes:'Febrero'
          },{
            id: 3,
            mes: 'Marzo'
          },{
            id: 4,
            mes: 'Abril'
          },{
            id: 5, 
            mes: 'Mayo'
          },{
            id: 6,
            mes: 'Junio'
          },{
            id: 7,
            mes: 'Julio'
          },{
            id: 8,
            mes: 'Agosto'
          },{
            id: 9,
            mes: 'Septiembre'
          },{
            id: 10, 
            mes: 'Octubre'
          },{
            id: 11,
            mes: 'Noviembre'
          },{
            id: 12, 
            mes: 'Diciembre'
          }
        ]
      };
    case DR_DISTRIBUCION_SUGERIDA:
      total_entrega = 0
      return {
         ...state,
        recaudaciones: state.recaudaciones.map(r => {
          r.suma = 0;
          r.oficialias = r.oficialias.map(o => {
            var vendidas_oficialia = Number(String(o.entregadas_oficialia_mes_anterior).replace(/,/g , "")),
              vendidas_use = Number(String(o.entregadas_use_mes_anterior).replace(/,/g , "")),
              vendidas_interestatales = Number(String(o.entregadas_interestatales_mes_anterior).replace(/,/g , ""));
            
            o.actas_oficialia = vendidas_oficialia === 0? 0: formatMoney( (vendidas_oficialia) + ( 50 - (vendidas_oficialia + 50) % 50) );
            o.actas_use = vendidas_use === 0? 0:  formatMoney( vendidas_use  + ( 10 - (vendidas_use  % 10) ));
            o.actas_interestatales = vendidas_interestatales === 0? 0: formatMoney( vendidas_interestatales + ( 10 - (vendidas_interestatales % 10) ) );

            o.suma = Number(String(o.actas_oficialia).replace(/,|\D/g , "")) +
                Number(String(o.actas_use).replace(/,|\D/g , "")) +
                Number(String(o.actas_interestatales).replace(/,|\D/g , ""));
            r.suma += o.suma;
            o.suma = formatMoney(o.suma);
            return o;
          });

          total_entrega += r.suma;
          r.suma = formatMoney(r.suma);
          return r;
        }),
        total_entrega: formatMoney(total_entrega)
      }
    case DR_GUARDAR:
      recaudaciones = null;
      data = action.payload[1].objects;
      total_entrega = 0
      if(action.payload && action.payload.length > 1) {
        recaudaciones = action.payload[0].objects && action.payload[0].objects.length > 0
        ? action.payload[0].objects.map(r => {
          r.active = 0;
          let rec_acnterior = state.recaudaciones.filter(re => Number(re.id) === Number(r.id));
          r.oficialias = r.oficialias.map(o => {
            let actas = getDistribucion(o, state.ventas);
            o.vendidas_oficialia = actas.actas_oficialia;
            o.vendidas_use = actas.actas_use;
            o.vendidas_interestatales = actas.actas_interestatales;
            o.actas_oficialia = '0';
            o.actas_use = '0';
            o.actas_interestatales = '0';
            let ofi = null;
            if(rec_acnterior && rec_acnterior.length === 1)
              ofi = rec_acnterior[0].oficialias.filter(ofis => ofis.id === o.id);
            if(ofi && ofi.length === 1) {
              ofi = ofi[0];
              o.vendidas_oficialia_mes_anterior = ofi.vendidas_oficialia_mes_anterior;
              o.vendidas_use_mes_anterior = ofi.vendidas_use_mes_anterior;
              o.vendidas_interestatales_mes_anterior = ofi.vendidas_interestatales_mes_anterior;
              o.entregadas_oficialia_mes_anterior = ofi.entregadas_oficialia_mes_anterior;
              o.entregadas_use_mes_anterior = ofi.entregadas_use_mes_anterior;
              o.entregadas_interestatales_mes_anterior = ofi.entregadas_interestatales_mes_anterior;
            }
            return o;
          });
          return r;
        }): null;

        if(recaudaciones && data.length > 0 && data[0].entregas.length > 0) {
          distribucion_id = data[0].id;
          distribucion_uri = data[0].resource_uri;
          for (let j = 0; j < recaudaciones.length; j++) {
            recaudaciones[j].suma = 0;
            for (let k = 0; k < recaudaciones[j].oficialias.length; k++) {
              let o = recaudaciones[j].oficialias[k];
              let e = data[0].entregas.filter(ent => ent.oficialia === o.resource_uri);
              if(e.length > 0) {
                let entrega = e[0];
                o.actas_interestatales = entrega.actas_interestatales? formatMoney(Number(entrega.actas_interestatales)): '';
                o.actas_oficialia = entrega.actas_oficialia? formatMoney(Number(entrega.actas_oficialia)): '';
                o.actas_use = entrega.actas_use? formatMoney(Number(entrega.actas_use)): '';
                o.entrega_id = entrega.id;
                o.entrega_uri = entrega.resource_uri;
                o.suma = Number(String(o.actas_oficialia).replace(/,|\D/g , "")) +
                  Number(String(o.actas_use).replace(/,|\D/g , "")) +
                  Number(String(o.actas_interestatales).replace(/,|\D/g , ""));
                recaudaciones[j].suma += o.suma;
                o.suma = formatMoney(o.suma);
              }
            }
            total_entrega += recaudaciones[j].suma;
            recaudaciones[j].suma = formatMoney(recaudaciones[j].suma);
          }
        }
      }
      return {
         ...state,
        distribucion_id: distribucion_id,
        recaudaciones: recaudaciones,
        distribucion_uri: distribucion_uri,
        total_entrega: formatMoney(total_entrega)
      }
    case DR_PAGE_UNLOADED:
      return {};
    case UPDATE_FIELD_OFICIALIA:
      //let r = state.recaudaciones.find(r => r.id === action.oficialia.rec_id);
      //let o = r.oficialias.find(o => o.id === action.oficialia.oficialia.id);
      //o = action.oficialia.oficialia;
      //console.log(o);
      total_entrega = 0;
      return {
        ...state,
        recaudaciones: state.recaudaciones.map(r => {
          r.suma = 0;
          if(r.id === action.oficialia.rec_id) 
            r.oficialias = r.oficialias.map(o => {
              if(o.id === action.oficialia.oficialia.id) {
                o = action.oficialia.oficialia;
                o.actas_oficialia = formatMoney(Number(String(o.actas_oficialia).replace(/,|\D/g , "")));
                o.actas_use = formatMoney(Number(String(o.actas_use).replace(/,|\D/g , "")));
                o.actas_interestatales = formatMoney(Number(String(o.actas_interestatales).replace(/,|\D/g , "")));
              }
              return o;
            });
          
          r.oficialias = r.oficialias.map(o => {
            o.suma = Number(String(o.actas_oficialia).replace(/,|\D/g , "")) +
            Number(String(o.actas_use).replace(/,|\D/g , "")) +
            Number(String(o.actas_interestatales).replace(/,|\D/g , ""));
            r.suma += o.suma;
            o.suma = formatMoney(o.suma);
            return o;
          });

          total_entrega += r.suma;
          r.suma = formatMoney(r.suma);
          return r;
        }),
        total_entrega: formatMoney(total_entrega)
      };
    case DR_CONTRACT_FORMS:
      return {
        ...state,
        recaudaciones: state.recaudaciones? state.recaudaciones.map(r => {
          r.active = 1; 
          return r;
        }): null
      };
    case DR_PAGE_LOAD_FOLIOS:
      action.payload = action.payload.map(e => e.fields);
      let folios = true, folio_final = null, folio_inicial1 = null, oficialias = [];
      state.recaudaciones.map(r => {
        r.oficialias.map(o => {
          oficialias.push(o);
          return o;
        });
        return r;
      })

      if(!oficialias || action.payload.length === 0) 
        return { ...state, folios_asignados: false };

      oficialias.map(o => { 
        let entregas = action.payload.filter(entrega => entrega.entrega === o.entrega_id);
        if(entregas && entregas.length === 1) {
          if(Number(entregas[0].folio_final) !== entregas[0].folio_inicial-1)
            folio_final = formatMoney(Number(entregas[0].folio_final));
        }
        else if(entregas && entregas.length > 1)
          folio_inicial1 = formatMoney(Number(entregas[0].folio_inicial));

        if(Number(String(o.suma).replace(/,|\D/g , "")) > 0 && !folio_final && !folio_inicial1)
          folios = false;
        return o;
      });
      return {
        ...state,
        folios_asignados: folios
      };
    case DR_EXPAND_FORMS:
      return {
        ...state,
        recaudaciones: state.recaudaciones? state.recaudaciones.map(r => {
          r.active = 0; 
          return r;
        }): null
      };
    case DR_TOGGLE_FORM:
      //let r = state.recaudaciones.find(r => r.id === action.recaudacion.id);
      //r.active = r.active == 0? 1: 0;
      //console.log(action.recaudacion);
      return {
        ...state,
        recaudaciones: state.recaudaciones? state.recaudaciones.map(r => {
          if(r.id === action.recaudacion.id)
            r.active = r.active === 0? 1: 0;
          return r;
        }): null
      };
    default:
      return state;
  }
};