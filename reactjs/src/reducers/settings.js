import {
  SETTINGS_SAVED,
  SETTINGS_PAGE_UNLOADED,
  ASYNC_START,
  ASYNC_END
} from '../constants/actionTypes';

export default (state = {}, action) => {
  var keys = {37: 1, 38: 1, 39: 1, 40: 1};

  function preventDefault(e) {
    e = e || window.event;
    if (e.preventDefault)
      e.preventDefault();
    e.returnValue = false;  
  }

  function preventDefaultForScrollKeys(e) {
    if (keys[e.keyCode]) {
      preventDefault(e);
      return false;
    }
  }
  function disableScrolling() { 
    if (window.addEventListener) // older FF
      window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove  = preventDefault; // mobile
    window.onscroll = () => window.scrollTo(0, 0);
    document.onkeydown  = preventDefaultForScrollKeys;
  };
  function enableScrolling() { 
    if (window.removeEventListener)
      window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null; 
    window.onwheel = null; 
    window.ontouchmove = null;  
    window.onscroll = null;
    document.onkeydown = null;  
  };
  switch (action.type) {
    case SETTINGS_SAVED:
      return {
        ...state,
        inProgress: false,
        errors: action.error ? action.payload.errors : null
      };
    case SETTINGS_PAGE_UNLOADED:
      return {};
    case ASYNC_START:
      disableScrolling();
      return {
        ...state,
        inProgress: true
      };
    case ASYNC_END:
      enableScrolling()
      return {
        ...state,
        inProgress: false
      };
    default:
      return state;
  }
};
