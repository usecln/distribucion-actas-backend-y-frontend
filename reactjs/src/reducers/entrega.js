import {
  ER_PAGE_LOADED,
  ER_PAGE_UNLOADED,
  ER_GUARDAR,
  UPDATE_FIELD_OFICIALIA,
  ER_PAGE_LOAD_FOLIOS
} from '../constants/actionTypes';

export default (state = {}, action) => {
  function formatMoney(n) {
    var c = 0, d = ".", t = ",", s = n < 0 ? "-" : "", 
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c), 10) ), 
    j = i.length;
    j = j > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
  };
  
  switch (action.type) {
    case ER_PAGE_LOADED:
      var oficialias = [], entregas = [], distribucion_id = null, total = 0;
      var data = action.payload[1].objects;
      if(!data[0]) {
        alert("No existe información");
        //window.location.replace('/');
        return { ...state };
      }
      distribucion_id = data[0].id;
      if(action.payload && action.payload.length > 0) {
        action.payload[0].objects.map(r => {
          r.oficialias = r.oficialias.map(o => {
            let e = data[0].entregas.filter(ent => ent.oficialia === o.resource_uri);

            o.actas_oficialia = 0;
            o.actas_interestatales = 0;
            o.actas_use = 0;

            if(e.length > 0) {
              let entrega = e[0];
              total += Number(entrega.actas_interestatales) + Number(entrega.actas_use) + Number(entrega.actas_oficialia);
              o.suma = formatMoney(Number(entrega.actas_interestatales) + Number(entrega.actas_use) + Number(entrega.actas_oficialia));
              o.actas_interestatales = formatMoney(Number(entrega.actas_interestatales));
              o.actas_use = formatMoney(Number(entrega.actas_use));
              o.actas_oficialia = formatMoney(Number(entrega.actas_oficialia));
              o.entrega_id = entrega.id;
              o.entrega_uri = entrega.resource_uri;
            }
            else {
              o.actas_interestatales = 0;
              o.actas_use = 0;
              o.actas_oficialia = 0;
              o.suma = 0;
            }
            oficialias.push(o);
            if(o.entrega_id)
              entregas.push(o.entrega_id);
            return o;
          });
          return r;
        });
      }
      return {
        ...state,
        mes: action.payload[2],
        ano: action.payload[3],
        total: formatMoney(total),
        distribucion_id,
        meses: [
          {
            id: 1,
            mes:'Enero'
          },{
            id: 2, 
            mes:'Febrero'
          },{
            id: 3,
            mes: 'Marzo'
          },{
            id: 4,
            mes: 'Abril'
          },{
            id: 5, 
            mes: 'Mayo'
          },{
            id: 6,
            mes: 'Junio'
          },{
            id: 7,
            mes: 'Julio'
          },{
            id: 8,
            mes: 'Agosto'
          },{
            id: 9,
            mes: 'Septiembre'
          },{
            id: 10, 
            mes: 'Octubre'
          },{
            id: 11,
            mes: 'Noviembre'
          },{
            id: 12, 
            mes: 'Diciembre'
          }
        ],
        oficialias,
        folios_asignados: false,
        entregas: entregas
      };
    case ER_PAGE_LOAD_FOLIOS:
      action.payload = action.payload.map(e => e.fields);
      let folios = true;
      if(!state.oficialias || action.payload.length === 0) 
        return { ...state, folios_asignados: false };
      return {
        ...state,
        oficialias: state.oficialias.map(o => { 
          let entregas = action.payload.filter(entrega => entrega.entrega === o.entrega_id);
          if(entregas && entregas.length === 1) {
            if(Number(entregas[0].folio_final) !== entregas[0].folio_inicial-1) {
              o.folio_inicial = formatMoney(Number(entregas[0].folio_inicial));
              o.folio_final = formatMoney(Number(entregas[0].folio_final));
            }
          }
          else if(entregas && entregas.length > 1)
          {
            o.folio_inicial1 = formatMoney(Number(entregas[0].folio_inicial));
            o.folio_final1 = formatMoney(Number(entregas[0].folio_final));
            o.folio_inicial2 = formatMoney(Number(entregas[entregas.length - 1].folio_inicial));
            o.folio_final2 = formatMoney(Number(entregas[entregas.length - 1].folio_final));
            o.multiple_lotes = true;
          }

          if(Number(String(o.suma).replace(/,|\D/g , "")) > 0 && !o.folio_final && !o.folio_inicial1)
            folios = false;
          return o;
        }),
        folios_asignados: folios
      };
    case ER_GUARDAR:
      return {
         ...state,
      }
    case ER_PAGE_UNLOADED:
      return {};
    case UPDATE_FIELD_OFICIALIA:
      return {
        ...state,
      };
    default:
      return state;
  }
};