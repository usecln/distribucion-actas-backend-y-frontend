import {
  APP_LOAD,
  REDIRECT,
  LOGOUT,
  SETTINGS_SAVED,
  LOGIN,
  REGISTER,  
  HOME_PAGE_UNLOADED,
  PROFILE_PAGE_UNLOADED,
  SETTINGS_PAGE_UNLOADED,
  LOGIN_PAGE_UNLOADED,
  REGISTER_PAGE_UNLOADED,
  DR_PAGE_UNLOADED,
  EE_PAGE_UNLOADED,
  ER_PAGE_UNLOADED,
  LOTES_PAGE_UNLOADED,
  LOTES_PAGE_LOADED,
  EE_PAGE_LOADED,
  ER_PAGE_LOADED,
  DR_PAGE_LOADED,
} from '../constants/actionTypes';

const defaultState = {
  appName: 'Actas USE',
  token: null,
  viewChangeCounter: 0
};

export default (state = defaultState, action) => {
  switch (action.type) {
    case EE_PAGE_LOADED:
      return {
        ...state,
        active: 'entrega_especial' 
      }
    case LOTES_PAGE_LOADED:
      return {
        ...state,
        active: 'lotes' 
      }
    case ER_PAGE_LOADED:
      return {
        ...state,
        active: 'entrega_regular' 
      }
    case DR_PAGE_LOADED:
      return {
        ...state,
        active: 'distribucion_regular' 
      }
    case APP_LOAD:
    const user = action.payload ? action.payload.user : null;
      return {
        ...state,
        token: action.token || null,
        appLoaded: true,
        currentUser: user,
        redirectTo: user? '/lotes' : '/login',
        active: user? 'lotes' : 'login',
        meses: [
          {
            id: 1,
            mes:'Enero'
          },{
            id: 2, 
            mes:'Febrero'
          },{
            id: 3,
            mes: 'Marzo'
          },{
            id: 4,
            mes: 'Abril'
          },{
            id: 5, 
            mes: 'Mayo'
          },{
            id: 6,
            mes: 'Junio'
          },{
            id: 7,
            mes: 'Julio'
          },{
            id: 8,
            mes: 'Agosto'
          },{
            id: 9,
            mes: 'Septiembre'
          },{
            id: 10, 
            mes: 'Octubre'
          },{
            id: 11,
            mes: 'Noviembre'
          },{
            id: 12, 
            mes: 'Diciembre'
          }
        ]
      };
    case REDIRECT:
      return {
        ...state,
        redirectTo: null
      };
    case LOGOUT:
      return {
        ...state,
        redirectTo: '/',
        token: null,
        currentUser: null
      };
    case SETTINGS_SAVED:
      return {
        ...state,
        redirectTo: action.error ? null : '/lotes',
        active: action.error? 'login' : 'lotes',
        currentUser: action.error ? null : action.payload.user
      };
    case LOGIN:
    case REGISTER:
    console.log('aquí esta el problema');
      return {
        ...state,
        redirectTo: action.error ? null : '/lotes',
        active: action.error? 'login' : 'lotes',
        token: action.error ? null : action.payload.token,
        currentUser: action.error ? null : action.payload.user
      };
    case HOME_PAGE_UNLOADED:
    case DR_PAGE_UNLOADED:
    case EE_PAGE_UNLOADED:
    case ER_PAGE_UNLOADED:
    case LOTES_PAGE_UNLOADED:
    case PROFILE_PAGE_UNLOADED:
    case SETTINGS_PAGE_UNLOADED:
    case LOGIN_PAGE_UNLOADED:
    case REGISTER_PAGE_UNLOADED:
      return { 
        ...state, 
        viewChangeCounter: state.viewChangeCounter + 1 
      };
    default:
      return state;
  }
};
