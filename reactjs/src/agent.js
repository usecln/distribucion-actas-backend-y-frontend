import superagentPromise from 'superagent-promise';
import _superagent from 'superagent';

const superagent = superagentPromise(_superagent, Promise);

const API_ROOT = 'http://localhost:80';

const responseBody = res => res.body;

let token = null;
const tokenPlugin = req => {
  if (token) {
    req.set('Authorization', `ApiKey ${token}`);
  }
}

const requests = {
  get: url => superagent.get(API_ROOT + '/api/v1/' + url).use(tokenPlugin).then(responseBody),
  post: (url, body) => superagent.post(API_ROOT + '/api/v1/' + url + '/', body).use(tokenPlugin).then(responseBody),
  patch: (url, body) => superagent.patch(API_ROOT + '/api/v1/' + url + '/', body).use(tokenPlugin).then(responseBody),
  delete: url => superagent.del(API_ROOT + '/api/v1/' + url + '/').use(tokenPlugin).then(responseBody)
};

const auth_requests = {
  get: url => superagent.get(API_ROOT + '/auth/v1/' + url + '/').use(tokenPlugin).then(responseBody),
  put: (url, body) => superagent.put(API_ROOT + '/auth/v1/' + url + '/', body).use(tokenPlugin).then(responseBody),
  post: (url, body) => superagent.post(API_ROOT + '/auth/v1/' + url + '/', body).use(tokenPlugin).then(responseBody),
}

const Auth = {
  current: () => auth_requests.get('get_user').catch(err => err),
  login: (email, password) => auth_requests.post('login', { email, password }).catch(err => err),
  register: (username, email, password) => auth_requests.post('register', { username, email, password }).catch(err => err),
  save: user => auth_requests.put('user', { user }).catch(err => err)
};

const Distribucion = {
  recaudaciones: () => requests.get('recaudacion/'),
  distribucion: (user, dia, mes, anio) => requests.get('distribucion/?user=' + user + '&dia=' + (dia? dia: 'None') + '&mes=' + mes + '&anio=' + anio + '&limit=0'),
  ventas: (user, dia, mes, anio) => requests.get('distribucion/actas_utilizadas/?mes=' + mes + '&anio=' + anio + '&limit=0'),
  createDistribucion: (user, dia, mes, anio) => requests.post('distribucion', { user: '/auth/v1/user/' + user + '/', dia, mes, anio, entregas: [] }),
  borraDistribucion: distribucion => requests.delete(`cancela_distribucion/${distribucion}`),
  addEntregas: entregas => requests.patch('entrega', entregas)
};

const Entrega = {
  asignaFolios: (entregas, username, tipo_lote) => requests.post('asigna_folios', { entregas, username, tipo_lote }),
  getFolios: entregas => requests.post('get_folios', { entregas: entregas || [] }),
  oficialias: () => requests.get('oficialia/?limit=0'),
};

const Lotes = {
  getLotes: user => requests.get(`lotes/?user=${user}`),
};

const Profile = {
  get: username => requests.get(`/profiles/${username}`),
  getTipoLotes: user => requests.get('tipo/?user=' + user + '&limit=0'),
};

export default {
  Auth,
  Profile,
  Distribucion,
  Entrega,
  Lotes,
  API_ROOT,
  setToken: _token => token = _token
};
