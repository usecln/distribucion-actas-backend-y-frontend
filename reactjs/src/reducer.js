import auth from './reducers/auth';
import common from './reducers/common';
import home from './reducers/home';
import lotes from './reducers/lotes';
import distribucion from './reducers/distribucion';
import entrega from './reducers/entrega';
import entrega_especial from './reducers/entrega-especial';
import profile from './reducers/profile';
import settings from './reducers/settings';

import { combineReducers } from 'redux';

export default combineReducers({
  auth,
  common,
  home,
  lotes,
  distribucion,
  entrega,
  entrega_especial,
  profile,
  settings
});
