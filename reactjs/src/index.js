import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import store from './store';

import App from './components/App';
import Home from './components/Home/';
import Login from './components/Login';
import DistribucionRegular from './components/DistribucionRegular';
import EntregaRegular from './components/EntregaRegular';
import EntregaEspecial from './components/EntregaEspecial';
import Lotes from './components/Lotes';
import Profile from './components/Profile';
import Register from './components/Register';
import Settings from './components/Settings';

ReactDOM.render((
  <Provider store={store}>
    <Router history={hashHistory}>
      <Route path="/" component={App}>
        <IndexRoute component={Home} />
        <Route path="lotes" component={Lotes} />
        <Route path="distribucion_regular" component={DistribucionRegular} />
        <Route path="entrega_regular" component={EntregaRegular} />
        <Route path="entregas" component={EntregaEspecial} />
        <Route path="entrega_especial" component={EntregaEspecial} />
        <Route path="login" component={Login} />
        <Route path="register" component={Register} />
        <Route path="salir" component={Settings} />
        <Route path="@:username" component={Profile} />
      </Route>
    </Router>
  </Provider>
), document.getElementById('root'));
