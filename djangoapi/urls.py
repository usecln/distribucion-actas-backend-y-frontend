from django.conf.urls import url, include
from django.contrib import admin
from catalogos import resources as r
from django.views.generic.base import RedirectView, TemplateView

# TastyPie API
from tastypie.api import NamespacedApi

v1_api = NamespacedApi(api_name='v1')
v1_api.register(r.LoteResource())
v1_api.register(r.TipoResource())
v1_api.register(r.RecaudacionResource())
v1_api.register(r.OficialiaResource())
v1_api.register(r.DistribucionResource())
v1_api.register(r.EntregaResource())


auth = NamespacedApi(api_name='v1')
auth.register(r.UserResource())
auth.register(r.GroupResource())

urlpatterns = [
	url(r'^$', TemplateView.as_view(template_name='react-app.html')),
	url(r'^vista/', TemplateView.as_view(template_name='reporte.html')),
	url(r'^admin/', admin.site.urls),
	url(r'^api/', include(v1_api.urls)),
	url(r'^auth/', include(auth.urls)),
]