import os
import sys
reload(sys)
sys.setdefaultencoding('UTF8')
#import locale
#locale.setlocale(locale.LC_ALL, 'es')
#from djangoapi import DatabaseAppsRouter
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '9@rq7_7t6z_3mn8o6!*zx$=%4%5-@2k@ir27vzv8zo)m%05p81'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']
CORS_ORIGIN_ALLOW_ALL = True

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'catalogos',
    'tastypie',
    'corsheaders',
    'django_jinja'
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'djangoapi.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {

            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
    {
        "BACKEND": "django_jinja.backend.Jinja2",
        "APP_DIRS": True,
        'DIRS': [os.path.join(BASE_DIR, 'djangoapi/templetes')],
        "OPTIONS": {
            "match_extension": ".html",
            "match_regex": r"^(?!admin/).*",
            "app_dirname": "templates",
        }
    },
]

WSGI_APPLICATION = 'djangoapi.wsgi.application'

WEBPACK_LOADER = {
    'DEFAULT': {
        'BUNDLE_DIR_NAME': 'bundles/local/',  # end with slash
        'STATS_FILE': os.path.join(BASE_DIR, 'webpack-stats-local.json'),
    }
}

# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases
# Define the database manager to setup the various projects

DATABASES = {
    'default': {
        'ENGINE': 'sql_server.pyodbc',
        'NAME': 'AppDistribucionActas',
        'USER': 'sa',
        'PASSWORD': 'soporte',
        'HOST': r'10.10.127.203\SQLEXPRESS',
        'PORT': '',

        'OPTIONS': {
            'driver': 'SQL Server Native Client 11.0',
        },
    },
    'actas': {
        'ENGINE': 'sql_server.pyodbc',
        'NAME': 'AppDistribucionActas',
        'USER': 'sa',
        'PASSWORD': 'soporte',
        'HOST': r'10.10.127.203\SQLEXPRESS',
        'PORT': '',

        'OPTIONS': {
            'driver': 'SQL Server Native Client 11.0',
        },
    },
    'sic': {
        'ENGINE': 'sql_server.pyodbc',
        'NAME': 'sic',
        'USER': 'RC_CONS',
        'PASSWORD': 'Reg2016Civ',
        'HOST': r'10.10.112.86\useestatal',
        'PORT': '4700',

        'OPTIONS': {
            'driver': 'SQL Server Native Client 11.0',
        },
    },
}

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

# Agregamos los idiomas
LANGUAGE_CODE = 'es'
TIME_ZONE = 'America/Mazatlan'
DEFAULT_CHARSET = 'utf-8'

USE_I18N = True
USE_L10N = True
USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR)

# STATICFILES_DIRS = STATIC_ROOT