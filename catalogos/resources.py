# coding=utf-8
import os
import json
import string
import random
import datetime
import calendar
import xlsxwriter
from easy_pdf import rendering as r
from calendar import monthrange
from tastypie.models import ApiKey
from tastypie.http import HttpUnauthorized, HttpForbidden
from tastypie.utils import trailing_slash
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from tastypie.exceptions import BadRequest
from tastypie.resources import ModelResource, fields
from tastypie.authentication import BasicAuthentication, ApiKeyAuthentication, MultiAuthentication

from django.core.files.storage import default_storage
from django.db import connections
from django.core import serializers
from django.http import JsonResponse, HttpResponse
from django.db.models import Max
from django.conf.urls import url
from django.db.models import F
from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User, Group
from django.contrib.auth import authenticate, login, logout
from django.core import serializers

from catalogos.models import  Lote, TipoLote, Oficialia, Recaudacion, Distribucion, Entrega, Folio, Responsable #, TipoUsuario
from tastypie.authorization import Authorization

def dictfetchall(cursor): 
    "Returns all rows from a cursor as a dict" 
    desc = cursor.description 
    return [
            dict(zip([col[0] for col in desc], row)) 
            for row in cursor.fetchall() 
    ]

class RecaudacionResource(ModelResource):
	oficialias = fields.ToManyField('catalogos.resources.OficialiaResource', 'oficialia_set', full=True)
	class Meta:
		queryset = Recaudacion.objects.all()
		resource_name = 'recaudacion'
		authorization = Authorization()

class ResponsableResource(ModelResource):
	class Meta:
		queryset = Responsable.objects.all()
		resource_name = 'responsable'
		authorization = Authorization()

class OficialiaResource(ModelResource):
	recaudacion = fields.ToOneField(RecaudacionResource, 'recaudacion', full=False)
	responsable = fields.ToOneField(ResponsableResource, 'responsable', full=True, null=True)
	class Meta:
		queryset = Oficialia.objects.all()
		resource_name = 'oficialia'
		authorization = Authorization()

class DistribucionResource(ModelResource):
	user = fields.ToOneField('catalogos.resources.UserResource', 'user', full=False)
	entregas = fields.ToManyField('catalogos.resources.EntregaResource', 'entrega_set', full=True)
	class Meta:
		queryset = Distribucion.objects.all()
		resource_name = 'distribucion'
		authorization = Authorization()
		filtering = {
			'dia': ALL,
			'mes': ALL,
			'anio': ALL,
			'user': ALL,
			'entregas': ALL_WITH_RELATIONS
		}
	def override_urls(self):
		return [
			url(r'^distribucion/actas_utilizadas/$', self.wrap_view('actas_utilizadas'), name='api_utilizadas'),
			url(r'^cancela_distribucion/(?P<id>\w[\w/-]*)/$', self.wrap_view('cancela_distribucion'), name='api_cancela'),
		]
	def cancela_distribucion(self, request, **kwargs):
		self.method_check(request, allowed=['delete'])
		d = Distribucion.objects.get(pk=kwargs.pop('id'))
		e = Entrega.objects.filter(distribucion_id=d.id)
		def entregas(x):
			return x.id
		data = [entregas(x) for x in e]
		f = Folio.objects.values().filter(entrega_id__in=data).order_by('-id')
		for i, x in enumerate(f):
			l = Lote.objects.get(id=x.get('lote_id'))
			if x.get('folio_inicial') == l.folio_inicial:
				l.ultimo_folio = -1
			else:
				l.ultimo_folio = int(x.get('folio_inicial')) - 1
			l.save()
			# folio = Folio.objects.get(id=x.get('id'))
			# folio.delete()
		d.delete()
		return JsonResponse('borrado', safe=False)

	def actas_utilizadas(self, request, **kwargs):
		self.method_check(request, allowed=['get'])
		anio = request.GET['anio']
		mes = request.GET['mes']
		mes = mes if int(mes) > 9 else '0' + str(mes)
		if mes and anio:
			diasMes = 28

			inicial = inicial = '01/' + mes + '/' + anio
			final = str(list(monthrange(int(anio), int(mes)))[1]) + '/' + mes + '/' + anio

			query = "select distinct g.cve_oficialia, c.oficialia as oficialia, \
				( \
					select count(*) from nacimientos s(nolock) inner join tramites t(nolock) \
					on t.cve_oficialia=s.cve_oficialia and t.ano = s.ano and t.tramite = s.tramite \
					where s.ano = " + anio + " and t.status <> 'C'and s.fecha_registro >= convert(datetime, '" + inicial + " 00:00:00', 103) \
					and s.fecha_registro <= convert(datetime, '" + final + " 23:59:59', 103) and s.cve_oficialia = g.cve_oficialia \
				) + \
				( \
					select count(*) from matrimonios s(nolock) inner join tramites t(nolock) \
					on t.cve_oficialia = s.cve_oficialia and t.ano = s.ano and t.tramite=s.tramite \
					where s.ano = " + anio + " and t.status <> 'C'and s.fecha_registro >= convert(datetime, '" + inicial + " 00:00:00', 103) \
					and s.fecha_registro <= convert(datetime, '" + final + " 23:59:59', 103) and s.cve_oficialia=g.cve_oficialia \
				) + \
				( \
					select count(*) from defunciones s(nolock) inner join tramites t(nolock) \
					on t.cve_oficialia = s.cve_oficialia and t.ano=s.ano and t.tramite = s.tramite \
					where s.ano = " + anio + " and t.status <> 'C' and s.fecha_registro >= convert(datetime, '" + inicial + " 00:00:00', 103) \
					and s.fecha_registro <= convert(datetime, '" + final + " 23:59:59', 103) and s.cve_oficialia = g.cve_oficialia \
				) + \
				( \
					select count(*) from divorcios s(nolock) inner join tramites t(nolock) \
					on t.cve_oficialia = s.cve_oficialia and t.ano = s.ano and t.tramite = s.tramite \
					where s.ano = " + anio + " and t.status <> 'C' and s.fecha_registro >= convert(datetime, '" + inicial + " 00:00:00', 103) \
					and s.fecha_registro <= convert(datetime, '" + final + " 23:59:59', 103) and s.cve_oficialia = g.cve_oficialia \
				) + \
				( \
					select count(*) from adopciones s(nolock) inner join tramites t(nolock) \
					on t.cve_oficialia = s.cve_oficialia and t.ano = s.ano and t.tramite = s.tramite \
					where s.ano = " + anio + " and t.status <> 'C' and s.fecha_registro >= convert(datetime, '" + inicial + " 00:00:00', 103) \
					and s.fecha_registro <= convert(datetime, '" + final + " 23:59:59', 103) and s.cve_oficialia = g.cve_oficialia \
				) + \
				( \
					select count(*) from reconocimientos s(nolock) inner join tramites t(nolock) \
					on t.cve_oficialia = s.cve_oficialia and t.ano = s.ano and t.tramite = s.tramite \
					where s.ano = " + anio + " and t.status <> 'C' and s.fecha_registro >= convert(datetime, '" + inicial + " 00:00:00', 103) \
					and s.fecha_registro <= convert(datetime, '" + final + " 23:59:59', 103) and s.cve_oficialia = g.cve_oficialia \
				) + \
				( \
					select count(*) from inscripcion_sentencias s(nolock) inner join tramites t(nolock) \
					on t.cve_oficialia = s.cve_oficialia and t.ano = s.ano and t.tramite = s.tramite \
					where s.ano = " + anio + " and t.status <> 'C' and s.fecha_registro >= convert(datetime, '" + inicial + " 00:00:00', 103) \
					and s.fecha_registro <= convert(datetime, '" + final + " 23:59:59', 103) and s.cve_oficialia = g.cve_oficialia \
				) + \
				ISNULL( \
				( \
					select sum(s.cant) \
					from servicios s(nolock) inner join tramites t(nolock) \
					on t.cve_oficialia = s.cve_oficialia and t.ano = s.ano and t.tramite = s.tramite \
					where t.status <> 'C' \
					and t.fecha_sol >= convert(datetime, '" + inicial + " 00:00:00', 103) \
					and t.fecha_sol <= convert(datetime, '" + final + " 23:59:59', 103) \
						and s.cve_oficialia = g.cve_oficialia and s.ano = " + anio + " and s.cve_servicio in (22,56,57,58,59,60,61,62,63,64,65,67,68,90,95) \
					), 0) as actas_vendidas \
				from globales g(nolock) inner join cat_oficialias c(nolock) \
				on g.cve_oficialia = c.cve_oficialia \
				order by g.cve_oficialia"

			with connections['sic'].cursor() as cursor:
				#cursor = connections['sic'].cursor()
				cursor.execute(query)
				rows = dictfetchall(cursor)
			return JsonResponse(rows, safe=False)
		else:
			raise BadRequest('falta_fecha')
class EntregaResource(ModelResource):
	user = fields.ToOneField('catalogos.resources.UserResource', 'user', full=False)
	distribucion = fields.ToOneField(DistribucionResource, 'distribucion', full=False)
	oficialia = fields.ToOneField(OficialiaResource, 'oficialia', full=False)
	tipo = fields.ToOneField('catalogos.resources.TipoResource', 'tipo', full=True)
	class Meta:
		queryset = Entrega.objects.all()
		resource_name = 'entrega'
		authorization = Authorization()
#------------------------------------------------------------------------------------------------------
class GroupResource(ModelResource):
    class Meta:
        queryset = Group.objects.all()
        resource_name = 'group'
        authorization = Authorization()
        authentication = MultiAuthentication(BasicAuthentication(), ApiKeyAuthentication())
        always_return_data = True
        filtering = {
            'id': ALL,
            'name': ALL,
        }

class UserResource(ModelResource):
	groups = fields.ToManyField(GroupResource, 'groups', full=True, blank=True)
	class Meta:
		queryset = User.objects.all()
		resource_name = 'user'
		excludes = ['is_superuser', 'password']
		authentication = MultiAuthentication(BasicAuthentication(), ApiKeyAuthentication())
		authorization = Authorization()
	def override_urls(self):
		return [
			url(r'^get_user/$', self.wrap_view('get_user'), name='api_login'),
			url(r'^login/$', self.wrap_view('login'), name='api_login'),
			url(r'^logout/$', self.wrap_view('logout'), name='api_logout'),
			url(r'^tipo_lotes/$', self.wrap_view('tipo_lotes'), name='api_tipo_lotes')
		]
	def login(self, request, **kwargs):
		self.method_check(request, allowed=['post'])
		data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))
		email = data.get('email')
		password = data.get('password')

		options = list(range(10)) + list(string.ascii_letters)
		key = ''.join(map(str, random.sample(options, 10)))
		UserModel = get_user_model()
		try:
			user = UserModel.objects.get(email=email)
		except UserModel.DoesNotExist:
			try:
				user = UserModel.objects.get(username=email)
			except UserModel.DoesNotExist:
				return JsonResponse({'error': 'Usuario no encontrado' })
		if user.check_password(password):
			if user.is_active:
				login(request, user)

				def grupo(grupo):
					return { 'id': grupo.pk, 'grupo': grupo.name }

				user_json = { 'id': user.pk, 'name': user.first_name, 'lastname': user.last_name, 'email': user.email, 'username': user.username, 'grupos': map(grupo, user.groups.all()) }
				try:
					api_key = ApiKey.objects.get(user=user)
					#api_key.delete()
					#api_key = ApiKey.objects.create(key=key, user=user)
				except Exception as e:
					api_key = ApiKey.objects.create(key=key, user=user)

				return JsonResponse({ 'token' : api_key.key, 'user': user_json }) # temporal
			else:
				return JsonResponse({'error': 'El usuario no está activo' })
		else:
			return JsonResponse({'error': 'Contraseña incorrecta' })
	def logout(self, request, **kwargs):
		self.method_check(request, allowed=['get'])
		if request.user and request.user.is_authenticated():
			try:
				api_key = ApiKey.objects.get(user=request.user)
				api_key.delete()
			except Exception as e:
				pass
			logout(request)
			return self.create_response(request, { 'success' : True })
		else:
			return self.create_response(request, { 'success' : False }, HttpUnauthorized)
	def tipo_lotes(self, request, **kwargs):
		self.method_check(request, allowed=['post'])
		try:
			auth = request.META['HTTP_AUTHORIZATION']
			index = auth.index(' ')

			auth = auth[index + 1:]
			index = auth.index(':')
			username = auth[:index]
			key = auth[index + 1:]

			token = ApiKey.objects.get(user__username=username)
			if token.key != key:
				raise BadRequest()
		except Exception as e:
			raise BadRequest('Unauthorized')

		tipos = TipoUsuario.objects.filter(user__username=username).values('tipo')
		return JsonResponse({ 'tipos' : list(tipos) }, safe=False)
	def get_user(self, request, **kwargs):
		self.method_check(request, allowed=['get'])
		try:
			auth = request.META['HTTP_AUTHORIZATION']
			index = auth.index(' ')

			auth = auth[index + 1:]
			index = auth.index(':')
			username = auth[:index]
			key = auth[index + 1:]
			user = User.objects.get(username=username)
			token = ApiKey.objects.get(user=user)
			if token.key != key:
				raise BadRequest()
		except Exception as e:
			raise BadRequest('Unauthorized')
		
		def grupo(grupo):
			return { 'id': grupo.pk, 'grupo': grupo.name }

		user_json = { 'id': user.pk, 'name': user.first_name, 'lastname': user.last_name, 'email': user.email, 'username': user.username, 'grupos': map(grupo, user.groups.all()) }
		return JsonResponse({ 'token' : token.key, 'user': user_json })		
	def obj_create(self, bundle, request=None, **kwargs):
		try:
			options = list(range(10)) + list(string.ascii_letters)
			key = ''.join(map(str, random.sample(options, 10)))

			first_name=bundle.data.get('name')
			email=bundle.data.get('email')
			index=email.index('@')
			name=email[:index].replace("-", "").replace("_", "").replace(".", "")
			password=bundle.data.get('password')

			if not User.objects.filter(username=name).exists():
				user = User.objects.create_user(name, email=email, password=password, first_name=first_name, is_staff=False)
				user.save()
				ApiKey.objects.create(key=key, user=user)
				## regresar response usuario creado
			else:
				raise BadRequest('El usuario ya existe')
		except Exception as inst:
			raise BadRequest(inst)
	def delete_detail(self, request, **kwargs):
		try:
			User.objects.get(pk=kwargs.get('pk')).delete()
			return self.create_response(request, { 'success' : 'Usuario eliminado' })
		except Exception as nf:
			raise BadRequest(nf)

class TipoResource(ModelResource):
	def build_filters(self, filters=None, **kwargs):
		print(self)

		if filters is None:
			filters = {}

		orm_filters = super(ModelResource, self).build_filters(filters, **kwargs)

		if "user" in filters:
			# si se pide filtrar por usuario solo se deben mostrar lotes de los tipos asignados a los grupos a los que pertenece el usuario
			# grupos a los que pertenece el usuario
			user = User.objects.filter(pk=filters['user'])
			if len(user) > 0:
				grupos = Group.objects.filter(user=user[0])
				tipos_lotes = []
				for grupo in grupos:
					# tipos de actas asignadas al grupo
					tipos = TipoLote.objects.filter(grupo=grupo)
					for tipo in tipos:
						if not tipo.id in tipos_lotes:
							tipos_lotes.append(tipo.id)
				orm_filters["pk__in"] = tipos_lotes

		return orm_filters
	class Meta:
		queryset = TipoLote.objects.all()
		resource_name = 'tipo'
		authorization = Authorization()

class TipoUsuarioResource(ModelResource):
	user = fields.ToOneField(UserResource, 'user', full=True)
	tipo = fields.ToOneField(TipoResource, 'tipo', full=True)
	def __str__(self):
		return '%s, %s' % (self.user.first_name, self.tipo.tipo)

class LoteResource(ModelResource):
	tipo = fields.ToOneField(TipoResource, 'tipo', full=True)
	def build_filters(self, filters=None, **kwargs):
		print(self)

		if filters is None:
			filters = {}

		orm_filters = super(ModelResource, self).build_filters(filters, **kwargs)

		if "user" in filters:
			# si se pide filtrar por usuario solo se deben mostrar lotes de los tipos asignados a los grupos a los que pertenece el usuario
			# grupos a los que pertenece el usuario
			user = User.objects.filter(pk=filters['user'])
			if len(user) > 0:
				grupos = Group.objects.filter(user=user[0])
				tipos_lotes = []
				for grupo in grupos:
					# tipos de actas asignadas al grupo
					tipos = TipoLote.objects.filter(grupo=grupo)
					for tipo in tipos:
						if not tipo.id in tipos_lotes:
							tipos_lotes.append(tipo.id)
				orm_filters["tipo_id__in"] = tipos_lotes

		return orm_filters
	class Meta:
		authentication = MultiAuthentication(BasicAuthentication(), ApiKeyAuthentication())
		queryset = Lote.objects.exclude(ultimo_folio=F('folio_final'))
		always_return_data = True 
		resource_name = 'lotes'
		authorization = Authorization()
		filtering = {
			'tipo': ALL
		}
	def override_urls(self):
		return [
			url(r'^asigna_folios/$', self.wrap_view('asigna_folios'), name='asigna_folios'),
			url(r'^get_folios/$', self.wrap_view('get_folios'), name='get_folios'),
			url(r'^get_pdf/$', self.wrap_view('get_pdf'), name='get_pdf'),
			url(r'^get_entrega/$', self.wrap_view('get_entrega'), name='get_entrega'),
		]
	def obj_create(self, bundle, request=None, **kwargs):
		try:
			tipo=bundle.data.get('tipo')
			user=bundle.data.get('user')
			folio_inicial=bundle.data.get('folio_inicial')
			folio_final=bundle.data.get('folio_final')
			max_folio_final = Lote.objects.filter(user_id=user.get('pk'), tipo_id=tipo.get('pk')).aggregate(Max('folio_final'))
			if max_folio_final.get('folio_final__max') is None:
				if folio_inicial > folio_final:
					raise BadRequest('Folio inicial debe ser menor a folio final')
			else:
				if folio_inicial <= max_folio_final.get('folio_final__max'):
					raise BadRequest('%s %s' % ('El folio inicial debe ser mayor a el utlimo folio final: ', max_folio_final.get('folio_final__max')))
				else:
					if folio_inicial > folio_final:
						raise BadRequest('Folio inicial debe ser menor a folio final')
			bundle.obj = Lote()
			bundle = self.full_hydrate(bundle)
			bundle.obj.user_id = user.get('pk')
			bundle.obj.tipo_id = tipo.get('pk')
			bundle.obj.folio_inicial = folio_inicial
			bundle.obj.folio_final = folio_final
			return self.save(bundle)
		except Exception as inst:
			raise BadRequest(inst)
			#raise BadRequest('El lote indicado ya existe o ya fue utilizado por este usuario')
	def asigna_folios(self, request, **kwargs):
		self.method_check(request, allowed=['post'])
		self.is_authenticated(request)

		data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))
		entregas = data.get('entregas')
		# username = data.get('username')
		tipo_lote = data.get('tipo_lote')
		print(tipo_lote)

		# user = User.objects.get(username=username)

		e = Entrega.objects.filter(id__in=entregas) # entregas a las que se les va a asiganr folios
		f = Folio.objects.filter(entrega_id__in=entregas) # folios ya asignados
		
		tipos_lotes = [tipo_lote]

		"""
		tipos_lotes.append()
		if user is not None:
			grupos = Group.objects.filter(user=user)
			for grupo in grupos:
				tipos = TipoLote.objects.filter(grupo=grupo)
				for tipo in tipos:
					if not tipo.id in tipos_lotes:
						tipos_lotes.append(tipo.id)
		"""

		for x in e:
			asignado = f.filter(entrega_id=x.pk)

			if len(asignado) == 0:
				lotes = Lote.objects.filter(tipo__in=tipos_lotes).exclude(ultimo_folio=F('folio_final')).order_by('folio_inicial')
				suma_disponible = 0
				
				aEntregar = x.actas_oficialia + x.actas_use + x.actas_interestatales

				for lote in lotes:
					if lote.ultimo_folio != -1:
						suma_disponible += lote.folio_final - lote.ultimo_folio
					elif lote.ultimo_folio == -1:
						suma_disponible += lote.folio_final - lote.folio_inicial + 1
					
					if suma_disponible > aEntregar:
						break

				print(aEntregar)
				print(suma_disponible)
				if aEntregar > suma_disponible:
					return JsonResponse({'error': 'No hay folios suficientes para asignar, agregue lotes primero, luego de clic en asignar folios'})

				for lote in lotes:
					if (lote.ultimo_folio != -1 and lote.folio_final - lote.ultimo_folio >= aEntregar) or (lote.ultimo_folio == -1 and lote.folio_final - lote.folio_inicial + 1 >= aEntregar):
						# si alcanzan los folios del lote actual para entregar a la entrega actual
						if lote.ultimo_folio == -1: # el lote no ha sido usado
							Folio.objects.create(entrega_id=x.pk, lote_id=lote.pk, folio_inicial=lote.folio_inicial, folio_final= lote.folio_inicial + aEntregar - 1)
							lote.ultimo_folio = lote.folio_inicial + aEntregar - 1
						else: # a el lote ya le faltan hojas
							Folio.objects.create(entrega_id=x.pk, lote_id=lote.pk, folio_inicial=lote.ultimo_folio + 1, folio_final= lote.ultimo_folio + aEntregar)
							lote.ultimo_folio = lote.ultimo_folio + aEntregar
						aEntregar = 0
						lote.save()
						break
					else:
						# el lote actual se consumira por completo y no acompletará los folios requeridos por la entrega actual
						if lote.ultimo_folio == -1: # lote nuevo
							Folio.objects.create(entrega_id=x.pk, lote_id=lote.pk, folio_inicial=lote.folio_inicial, folio_final= lote.folio_final)
							aEntregar = aEntregar - (lote.folio_final - lote.folio_inicial + 1)
							lote.ultimo_folio = lote.folio_final
						else: # lote usado
							Folio.objects.create(entrega_id=x.pk, lote_id=lote.pk, folio_inicial=lote.ultimo_folio + 1, folio_final= lote.folio_final)
							aEntregar = aEntregar - (lote.folio_final - lote.ultimo_folio)
							lote.ultimo_folio = lote.folio_final
						lote.save()
					if aEntregar == 0: # ya se acompletaron los folios requeridos por la entrega actual
						break
				if aEntregar > 0: # aun hay actas a entregar y ya se acabaron los lotes
					return JsonResponse({'error': 'No hay folios suficientes para asignar, agregue lotes primero, luego de clic en asignar folios'})
			else:
				pass
		return JsonResponse({'success': True})
	def get_folios(self, request, **kwargs):
		self.method_check(request, allowed=['post'])
		self.is_authenticated(request)

		data = self.deserialize(request, request.body, format = request.META.get('CONTENT_TYPE', 'application/json'))
		entregas = data.get('entregas')
		
		f = Folio.objects.filter(entrega_id__in=entregas)

		folios = []

		for folio in f:
			folios.append({
				'id': folio.pk,
				'entrega': folio.entrega_id,
				'folio_final': folio.folio_final,
				'folio_inicial': folio.folio_inicial,
				'tipo_lote': folio.lote.tipo.tipo
			})

		print(folios)

		return JsonResponse(folios, safe=False)

	def get_pdf(self, request, **kwargs):
		self.method_check(request, allowed=['get'])
		distribucion = request.GET['distribucion']
		#user = request.GET['user']
		d = Distribucion.objects.values().get(pk=distribucion)
		mes = calendar.month_name[int(d.get('mes'))].title()
		anio = d.get('anio')
		fecha = mes + ' ' + str(anio)
		if d.get('dia') is not None:
			fecha = str(d.get('dia')) + ' de ' + mes + ' del ' + str(anio )
		e = Entrega.objects.values().filter(distribucion_id=distribucion)
		o = Oficialia.objects.values().all()
		r = Recaudacion.objects.values().all().order_by('id')
		
		def get_id(entrega):
			return entrega.get('id')
		f = Folio.objects.values().filter(entrega_id__in=[get_id(x) for x in e])
		if f[0] is not None:
			l = Lote.objects.values().get(pk=f[0].get('lote_id'))
			u = User.objects.values().get(pk=l.get('user_id'))
			t = TipoLote.objects.values().get(pk=l.get('tipo_id'))
			ls = Lote.objects.values().filter(user_id=u.get('id')).exclude(ultimo_folio=F('folio_final'))
			folios_restantes = 0
			for lote in ls:
				if int(lote.get('ultimo_folio')) == -1:
					folios_restantes = folios_restantes + (lote.get('folio_final') - lote.get('folio_inicial'))
				else:
					folios_restantes = folios_restantes + (lote.get('folio_final') - lote.get('ultimo_folio'))
		else:
			return JsonResponse({'error': 'No hay folios asignados'}, safe=False)

		def asigna_oficialia_y_folios(entrega):
			oficialia_x = o.get(id=entrega.get('oficialia_id'))
			folio_x = f.filter(entrega_id=entrega.get('id')).order_by('id')
			entrega['oficialia'] = oficialia_x
			entrega['folios'] = list(folio_x)
			return entrega
		data = [asigna_oficialia_y_folios(x) for x in e]
		def separa_recaudaciones(recaudacion):
			oficialias = [x for x in data if x.get('oficialia').get('recaudacion_id') == recaudacion.get('id')]
			recaudacion['oficialias'] = oficialias
			return recaudacion

		final = [separa_recaudaciones(x) for x in r]
		# Create an new Excel file and add a worksheet.
		workbook = xlsxwriter.Workbook('distribucionX.xlsx')
		worksheet = workbook.add_worksheet(fecha)

		# Create a format to use in the merged range.
		merge_format_header = workbook.add_format({
			'bold': 1,
			'border': 1,
			'align': 'center',
			'valign': 'vcenter',
			'fg_color': '#f3be87' #f3be87
		})
		merge_format_header_left = workbook.add_format({
			'bold': 1,
			'border': 1,
			'align': 'left',
			'valign': 'vcenter',
			'fg_color': '#f3be87' #f3be87
		})
		merge_format_header.set_text_wrap()
		text_right_header_money = workbook.add_format({'bold': 1, 'border': 1, 'align': 'right', 'num_format': '#,##0', 'fg_color': '#f3be87'})

		merge_format_yellow = workbook.add_format({
			'bold': 1,
			'border': 1,
			'align': 'center',
			'valign': 'vcenter',
			'fg_color': '#FDFD96'
		})
		merge_format_yellow_money = workbook.add_format({
			'bold': 1,
			'border': 1,
			'align': 'right',
			'valign': 'vcenter',
			'fg_color': '#FDFD96',
			'num_format': '#,##0'
		})
		text_right = workbook.add_format({'align': 'right'})
		text_center = workbook.add_format({'align': 'center'})
		text_right_money = workbook.add_format({'align': 'right', 'num_format': '#,##0'})
		# printing
		## repetir header en cada pagina
		worksheet.repeat_rows(0, 3)
		## numero de pagina en footer
		worksheet.set_footer('&CPágina &P de &N')
		worksheet.set_margins(left=0.2, right=0.2, top=0.3, bottom=0.4)
		worksheet.fit_to_pages(1, 0) 
		
		# width of columns
		worksheet.set_row(0, 40)
		worksheet.set_column('B:B', 40)
		worksheet.set_column('C:C', 8)
		worksheet.set_column('D:D', 8)
		worksheet.set_column('E:E', 8)
		worksheet.set_column('F:F', 4)
		worksheet.set_column('G:G', 9)
		worksheet.set_column('H:H', 4)
		worksheet.set_column('I:I', 9)
		worksheet.set_column('J:J', 8)

		# header
		## esta formado por 4 renglones
		worksheet.merge_range('A1:A3', 'Oficialia', merge_format_header)
		worksheet.merge_range('B1:B3', 'Nombre', merge_format_header)
		worksheet.merge_range('C1:E1', t.get('tipo') + ' (' + fecha + ')', merge_format_header)
		worksheet.merge_range('F1:J1', 'Después de entregar esta liquidación quedarán en bodega ' + '{:,}'.format(int(folios_restantes)) + ' formatos', merge_format_header)
		
		worksheet.merge_range('C2:C3', 'Oficialia', merge_format_header)
		worksheet.merge_range('D2:D3', 'USE', merge_format_header)
		worksheet.merge_range('E2:E3', 'Inter estatales', merge_format_header)

		worksheet.merge_range('F2:G2', 'Folio inicial', merge_format_header)
		worksheet.merge_range('H2:I2', 'Folio final', merge_format_header)
		worksheet.write('J2', 'Subtotal', merge_format_header)
		
		worksheet.write('F3', 'A25', merge_format_header)
		worksheet.write('H3', 'A25', merge_format_header)
		worksheet.merge_range('A4:J4', 'Nota: La entrega de formatos a los oficiales y responsables de módulos depende de su liquidación.', merge_format_header_left)

		row = 5 #(row, column, val, format)
		total_recaudaciones = 0
		primer_folio = None
		ultimo_folio = None
		for i, recaudacion in enumerate(final): # recaudaciones
			suma_recaudacion = 0
			worksheet.merge_range('A'+ str(row) + ':J' + str(row), recaudacion.get('recaudacion'), merge_format_yellow)
			index_recaudacion = row - 1
			for j, oficialia in enumerate(recaudacion.get('oficialias')): # oficialias
				suma_oficialia = int(oficialia.get('actas_oficialia')) + int(oficialia.get('actas_use')) + int(oficialia.get('actas_interestatales'))
				suma_recaudacion = suma_recaudacion + suma_oficialia
				if oficialia.get('oficialia').get('cve_oficialia') is not None:
					worksheet.write(row, 0, str(oficialia.get('oficialia').get('cve_oficialia')), text_center)
				worksheet.write(row, 1, str(oficialia.get('oficialia').get('nombre')))
				worksheet.write(row, 2, int(oficialia.get('actas_oficialia')), text_right_money)
				worksheet.write(row, 3, int(oficialia.get('actas_use')), text_right_money)
				worksheet.write(row, 4, int(oficialia.get('actas_interestatales')), text_right_money)
				for k, folio in enumerate(oficialia.get('folios')): # folios
					worksheet.write(row, 5, 'A25', text_right)
					worksheet.write(row, 6, int(folio.get('folio_inicial')), text_right_money)
					worksheet.write(row, 7, 'A25', text_right)
					worksheet.write(row, 8, int(folio.get('folio_final')), text_right_money)
					worksheet.write(row, 9, int(folio.get('folio_final')) - int(folio.get('folio_inicial')) + 1, text_right_money)
					row = row + 1
					if i == k == j == 0:
						primer_folio = int(folio.get('folio_inicial'))

					if i == len(final) - 1 and j == len(recaudacion.get('oficialias')) - 1 and k == len(oficialia.get('folios')) - 1:
						ultimo_folio = int(folio.get('folio_final'))
			total_recaudaciones = total_recaudaciones + suma_recaudacion
			worksheet.write(index_recaudacion, 10, suma_recaudacion, merge_format_yellow_money)
			row = row + 1

		# if d.get('dia') is not None:
		worksheet.merge_range('A'+ str(row + 1) + ':J' + str(row + 1), 'Total de formas entregadas en ' + fecha, text_right)

		worksheet.write('K' + str(row + 1), total_recaudaciones, text_right_money)
		worksheet.write('J3', total_recaudaciones, text_right_header_money)
		worksheet.write('G3', primer_folio, text_right_header_money)
		worksheet.write('I3', ultimo_folio, text_right_header_money)
		workbook.close()
		fsock = open('distribucionX.xlsx',"rb")
		return HttpResponse(fsock, content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
	def get_entrega(self, request, **kwargs):
		self.method_check(request, allowed=['get'])
		dia = request.GET['dia']
		mes = request.GET['mes']
		ano = request.GET['ano']
		distribucion = request.GET['distribucion']

		d = Distribucion.objects.values().get(id=distribucion)
		e = Entrega.objects.values().filter(distribucion_id=distribucion)

		def get_id(entrega):
			return entrega.get('id')
		
		f = Folio.objects.values().filter(entrega_id__in=[get_id(x) for x in e])

		#return JsonResponse([get_id(x) for x in e], safe=False)

		# return self.create_response(request, int(d['mes']))

		def prepara_entregas(folio):
			l = Lote.objects.get(id=folio['lote_id'])
			en = Entrega.objects.get(id=folio['entrega_id'])
			of = Oficialia.objects.get(id=en.oficialia_id)
			f = {
				'oficialia': of.cve_oficialia,
				'actas_oficialia': en.actas_oficialia,
				'actas_use': en.actas_use,
				'actas_interestatales': en.actas_interestatales,
				'folio_inicial': "{:,}".format(folio.get('folio_inicial')),
				'folio_final': "{:,}".format(folio.get('folio_final'))
			}
			if of.cve_oficialia is None:
				f['oficialia'] = ''
			if l.tipo_id == 1:
				f['folio_inicial'] = 'A25 ' + f['folio_inicial'] 
				f['folio_final'] = 'A25 ' + f['folio_final'] 
			return f
		
		entregas = [prepara_entregas(x) for x in f]
		
		hojas = []
		entregasx = []
		for o in entregas:
			if len(entregasx) < 23:
				entregasx.append(o)
			else:
				hojas.append({
					'entregas': entregasx
				})
				entregasx = [o]

		if len(entregasx) > 0:
			hojas.append({
				'entregas': entregasx
			})
		
		def borrar_archivo(): 
			try:
				os.remove('static/comprobante.pdf')
			except OSError:
				pass

		meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
		params = { 
			'day': d['dia'],
			'month': meses[int(d['mes']) - 1],
			'year': d['anio'],
			'hojas': hojas
		}

		content = r.render_to_content_file('reporte.html', params)

		borrar_archivo()
		default_storage.save('static/comprobante.pdf', content)
		
		#fsock = open('static/comprobante.pdf',"rb")
		with open('static/comprobante.pdf', 'rb') as fid:
			filedata = fid.read()

		borrar_archivo()
		
		#return HttpResponse(fsock, content_type="application/pdf")
		return HttpResponse(filedata, content_type="application/pdf")