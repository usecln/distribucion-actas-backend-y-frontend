# coding=utf-8
from django.contrib import admin
from django.contrib.auth.models import Group
from django.db.models import F
from catalogos import models as m

admin.site.site_header = 'Panel de administración / Actas USE'

# Register your models here.
class TipoLoteAdmin(admin.ModelAdmin):
	list_display = ('grupo', 'tipo')
	def get_ordering(self, request):
		return ['id']

class LoteAdmin(admin.ModelAdmin):
	list_display = ('tipo', 'folio_inicial', 'folio_final')
	fields = ('tipo', 'folio_inicial', 'folio_final')
	def get_ordering(self, request):
		return ['id']
	def render_change_form(self, request, context, *args, **kwargs):
		user = request.user
		tipos_lotes = []
		
		# obtener grupos del usuario logeado
		if user.username != 'admin':
			# si no es admin solo puede agregar lotes de los tipos asignados a los grupos a los que pertenece el usuario
			# grupos a los que pertenece el usuario
			grupos = Group.objects.filter(user=user)
			for grupo in grupos:
				# tipos de actas asignadas al grupo
				tipos = m.TipoLote.objects.filter(grupo=grupo)
				for tipo in tipos:
					if not tipo.id in tipos_lotes:
						tipos_lotes.append(tipo.id)
			context['adminform'].form.fields['tipo'].queryset = m.TipoLote.objects.filter(id__in=tipos_lotes)
		return super(LoteAdmin, self).render_change_form(request, context, *args, **kwargs)
	def get_queryset(self, request):
		return super(LoteAdmin, self).get_queryset(request) 

class RecaudacionAdmin(admin.ModelAdmin):
	list_display = ('recaudacion',)
	def get_ordering(self, request):
		return ['id']

class OficialiaAdmin(admin.ModelAdmin):
	list_display = ('recaudacion', 'responsable', 'suplencia', 'nivel', 'cve_oficialia', 'nombre', 'act_oficialia', 'act_use', 'act_interestatales')
	def get_ordering(self, request):
		return ['recaudacion_id', 'nivel', 'cve_oficialia', 'nombre']

class DistribucionAdmin(admin.ModelAdmin):
	list_display = ('user', 'tipo', 'dia', 'mes', 'anio')
	def get_ordering(self, request):
		return ['user', 'anio', 'mes', 'dia']
	def get_queryset(self, request):
		return super(DistribucionAdmin, self).get_queryset(request).filter(user=request.user)
	def render_change_form(self, request, context, *args, **kwargs):
		user = request.user
		context['adminform'].form.fields['user'].queryset = m.User.objects.filter(id=user.id)
		return super(DistribucionAdmin, self).render_change_form(request, context, *args, **kwargs)

class EntregaAdmin(admin.ModelAdmin):
	list_display = ('distribucion', 'oficialia', 'actas_oficialia', 'actas_use', 'actas_interestatales')
	def get_ordering(self, request):
		return ['id']
	def get_queryset(self, request):
		return super(EntregaAdmin, self).get_queryset(request).filter(user=request.user)
	def render_change_form(self, request, context, *args, **kwargs):
		user = request.user
		context['adminform'].form.fields['user'].queryset = m.User.objects.filter(id=user.id)
		context['adminform'].form.fields['distribucion'].queryset = m.Distribucion.objects.filter(user=user)
		return super(EntregaAdmin, self).render_change_form(request, context, *args, **kwargs)

class FolioAdmin(admin.ModelAdmin):
	list_display = ('entrega', 'lote', 'folio_inicial', 'folio_final', 'actas_entregadas')
	def render_change_form(self, request, context, *args, **kwargs):
		user = request.user

		tipos_lotes = []
		
		# obtener grupos del usuario logeado
		if user.username != 'admin':
			# si no es admin solo puede agregar lotes de los tipos asignados a los grupos a los que pertenece el usuario
			# grupos a los que pertenece el usuario
			grupos = Group.objects.filter(user=user)
			for grupo in grupos:
				# tipos de actas asignadas al grupo
				tipos = m.TipoLote.objects.filter(grupo=grupo)
				for tipo in tipos:
					if not tipo.id in tipos_lotes:
						tipos_lotes.append(tipo.id)
			print(tipos_lotes)
			context['adminform'].form.fields['lote'].queryset = m.Lote.objects.filter(tipo__in=tipos_lotes)
			context['adminform'].form.fields['entrega'].queryset = m.Entrega.objects.filter(user=user)
		return super(FolioAdmin, self).render_change_form(request, context, *args, **kwargs)
	def get_ordering(self, request):
		return ['id']
	def get_queryset(self, request):
		return super(FolioAdmin, self).get_queryset(request).filter(entrega__user=request.user)

admin.site.register(m.TipoLote, TipoLoteAdmin)
admin.site.register(m.Lote, )
admin.site.register(m.Responsable, )
admin.site.register(m.Oficialia, OficialiaAdmin)
admin.site.register(m.Recaudacion, RecaudacionAdmin)

admin.site.register(m.Distribucion, DistribucionAdmin)
admin.site.register(m.Entrega, EntregaAdmin)
admin.site.register(m.Folio, FolioAdmin)
#admin.site.register(m.TipoUsuario)