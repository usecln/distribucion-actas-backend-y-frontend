# -*- coding: utf-8 -*-
# Generated by Django 1.11.15 on 2019-01-30 21:56
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogos', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='oficialia',
            name='cve_oficialia',
            field=models.CharField(blank=True, max_length=5, null=True),
        ),
    ]
