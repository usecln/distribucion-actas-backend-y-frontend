# coding=utf-8
from django.contrib.auth.models import User, Group
from django.db import models
from django.db.models import F
from django.forms import ModelForm, ValidationError

# Create your models here.
class Recaudacion(models.Model):
	recaudacion = models.CharField(max_length=100, unique=True)
	class Meta:
		verbose_name = "Recaudacion"
		verbose_name_plural = "Recaudaciones"
	def __str__(self):
		return self.recaudacion

class Responsable(models.Model):
	responsable = models.CharField(max_length=100, unique=True)
	class Meta:
		verbose_name = "Responsable"
		verbose_name_plural = "Responsables"
	def __str__(self):
		return self.responsable

class Oficialia(models.Model):
	cve_oficialia = models.CharField(max_length=5, null=True, blank=True)
	nombre = models.CharField(max_length=200)
	recaudacion = models.ForeignKey(Recaudacion, on_delete=models.CASCADE)

	responsable = models.ForeignKey(Responsable, on_delete=models.CASCADE, null=True, blank=True, default=None)
	suplencia = models.BooleanField(default=False)

	act_oficialia = models.DecimalField(max_digits=5, decimal_places=0, null=True, blank=True)
	act_use = models.DecimalField(max_digits=5, decimal_places=0, null=True, blank=True)
	act_interestatales = models.DecimalField(max_digits=5, decimal_places=0, null=True, blank=True)

	nivel = models.DecimalField(max_digits=2, decimal_places=0, default=1)

	def __str__(self):
		if self.cve_oficialia is None:
			return self.nombre
		else:
			return '%s %s' % (str(self.cve_oficialia), self.nombre)
	class Meta:
		ordering = ['recaudacion_id', 'nivel', 'cve_oficialia', 'nombre']

class TipoLote(models.Model):
	tipo = models.CharField(max_length=30, unique=True)
	grupo = models.ForeignKey(Group, on_delete=models.CASCADE)
	def __str__(self):
		return self.tipo

class Lote(models.Model):
	tipo = models.ForeignKey(TipoLote, on_delete=models.CASCADE)
	folio_inicial = models.DecimalField(max_digits=20, decimal_places=0)
	folio_final = models.DecimalField(max_digits=20, decimal_places=0)
	ultimo_folio = models.DecimalField(max_digits=20, decimal_places=0, default=-1)
	class Meta:
		unique_together = ("tipo", "folio_inicial", "folio_final")
	def __str__(self):
		return "%s: %s - %s" % (self.tipo, self.folio_inicial, self.folio_final)

class Distribucion(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	dia = models.DecimalField(max_digits=2, decimal_places=0, null=True, blank=True)
	mes = models.DecimalField(max_digits=2, decimal_places=0)
	anio = models.DecimalField(max_digits=4, decimal_places=0)
	class Meta:
		verbose_name="Distribución"
		verbose_name_plural="Distribuciones"
		unique_together = ('user', 'dia', 'mes', 'anio')
	def tipo(self):
		if self.dia is None:
			return 'Menusal'
		else:
			return 'Especial'
	def __str__(self):
		if self.dia is None:
			return 'Entrga regular %s/%s' % (str(self.mes), str(self.anio))
		else:
			return 'Entrega especial %s/%s/%s' % (str(self.dia), str(self.mes), str(self.anio))

class Entrega(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	tipo = models.ForeignKey(TipoLote, on_delete=models.CASCADE)
	distribucion = models.ForeignKey(Distribucion, on_delete=models.CASCADE)
	oficialia = models.ForeignKey(Oficialia, on_delete=models.CASCADE)
	actas_oficialia = models.DecimalField(max_digits=20, decimal_places=0, null=True, blank=True)
	actas_use = models.DecimalField(max_digits=20, decimal_places=0, null=True, blank=True)
	actas_interestatales = models.DecimalField(max_digits=20, decimal_places=0, null=True, blank=True)
	def actas_entregar(self):
		return self.actas_oficialia + self.actas_use + self.actas_interestatales
	class Meta:
		unique_together = ('distribucion', 'oficialia', 'tipo')
		ordering = ['oficialia__recaudacion_id', 'oficialia__nivel', 'oficialia__cve_oficialia', 'oficialia__nombre']
	def __str__(self):
		return '%s %s, %s' % (
			str(self.oficialia.cve_oficialia), 
			str(self.oficialia.nombre), 
			str(
				(0 if self.actas_oficialia is None else self.actas_oficialia) + 
				(0 if self.actas_use is None else self.actas_use) + 
				(0 if self.actas_interestatales is None else self.actas_interestatales)
			)
		)

class Folio(models.Model):
	entrega = models.ForeignKey(Entrega, on_delete=models.CASCADE)
	lote = models.ForeignKey(Lote, on_delete=models.CASCADE)
	folio_inicial = models.DecimalField(max_digits=20, decimal_places=0)
	folio_final = models.DecimalField(max_digits=20, decimal_places=0)
	class Meta:
		unique_together = ("entrega", "lote")

	def clean(self):
		from django.core.exceptions import ValidationError
		if self.lote.ultimo_folio == -1 or (self.lote.ultimo_folio != -1 and self.folio_inicial != self.lote.ultimo_folio + 1):
			if self.lote.folio_final == self.lote.ultimo_folio:
				raise ValidationError('Este registro ya no se puede editar, ya se entregaron todos los folios del lote, no se puede editar un registro una ves que ya asignaron folios')
			else:
				if self.lote.ultimo_folio == -1 and self.folio_inicial != self.lote.folio_inicial:
					raise ValidationError(
						'El Folio inicial debe ser %s' %
							str(self.lote.folio_inicial))
				elif self.lote.ultimo_folio != -1 and self.folio_inicial != self.lote.ultimo_folio + 1:
					raise ValidationError('El Folio inicial debe ser %s, no se puede editar un registro una ves que entregaron folios posteriores' %
						str(self.lote.ultimo_folio + 1))
		if self.folio_inicial > self.folio_final:
			raise ValidationError('El Folio inicial debe ser menor al Folio final')
		folios_pendientes = Folio.objects.filter(entrega__id=self.entrega.id)
		suma = 0
		if folios_pendientes.count() > 0:
			suma = 0
			for folios in folios_pendientes:
				suma = suma + folios.folio_final - folios.folio_inicial + 1
			if self.folio_final - self.folio_inicial > self.entrega.actas_entregar() - suma:
				raise ValidationError('No se puede entregar mas de %s folios, folios entregados %s de %s' % (str(self.entrega.actas_entregar() - suma), str(suma), str(self.entrega.actas_entregar())))
		else:
			if self.folio_final - self.folio_inicial > self.entrega.actas_entregar():
				raise ValidationError('No se puede entregar mas de %s folios %s' % (str(self.entrega.actas_entregar()), str(suma)))
		if self.folio_final > self.lote.folio_final:
			raise ValidationError('El Folio final no debe ser mayor a %s, el ultimo folio del lote seleccionado es %s!' % (str(self.lote.folio_final), str(self.lote.folio_final)))
	def actas_entregadas(self):
		return self.folio_final - self.folio_inicial + 1

	def save(self, *args, **kwargs):
		if self.lote.ultimo_folio == -1:
			self.lote.ultimo_folio = self.lote.folio_inicial + self.folio_final - self.folio_inicial
		else:
			self.lote.ultimo_folio = self.lote.ultimo_folio + self.folio_final - self.folio_inicial + 1
		self.lote.save()
		super(Folio, self).save(*args, **kwargs)
	def __str__(self):
		return '%s %s, %s - %s' % (str(self.entrega.oficialia.cve_oficialia), str(self.entrega.oficialia.nombre), str(self.folio_inicial), str(self.folio_final))

"""
class TipoUsuario(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE)
	tipo = models.ForeignKey(TipoLote, on_delete=models.CASCADE)
	class Meta:
		app_label = 'auth'
		verbose_name = "Tipo de usuario"
		verbose_name_plural = "Tipos de usurios"
		unique_together = (("user", "tipo"),)
	def __str__(self):
		return '%s, %s' % (self.user, self.tipo.tipo)
"""